<?php
$lang_menu="pl";
$subpage="marka";
$title="DuoLife Keratin Hair";
$background="inherit";
@include('inc/header.php');
?>

<body>
    <div id="component">
        <?php @include('inc/top.php'); ?>
    </div>
    <div id="hair2">
        <div class="hair2-content">
            <div class="hair2-boxes">
                <div class="hair2-box-text">
                    <h4>Dla kogo został stworzony Keratin Hair Complex</h4>
                    <div>
                        Wypadanie włosów to proces fizjologiczny, wynikający z naturalnego cyklu rozwojowego włosa. Niestety, nadmierne wypadanie sugeruje niekorzystne zmiany zachodzące w obrębie nie tylko włosów, ale i organizmu. W naszym ciele bowiem dochodzi do zaburzeń spowodowanych stresem, brakiem równowagi hormonalnej, działaniem czynników środowiskowych, ale i wiekiem oraz dziedziczeniem. Gdy skóra głowy staje się  słabiej ukrwiona, a kosmyki niedostatecznie odżywione, dochodzi do ich osłabienia, a co za tym idzie, okresowego lub przewlekłego wypadania. Włosy pozbawoine witalności tracą wytrzymałość i gęstość<br><br>
                        W przypadku procesu fizjologicznego, włosy wypadają z częstotliwością około 60-100 kosmyków na dobę. Na ich miejsce wzrastają nowe. Może zdarzyć się tak, że nawet te nowe będą kruche, łamliwe, pozbawione blasku i bez wyraźnej gęstości<br><br>
                        <b>Keratin Hair Complex</b> stworzyliśmy po to, by o swoje włosy mogly zadbać osoby zmagające się z cienkimi i delikatnymi włosami. To sosób na odżywienie i wzmocnienie, dzięki formule kompleksu keratynowego. Łączy on unikatowe wartości naturalnych wyciągów roślinnych.<br><br>
                        Składniki <b>Keratin Hair Complex</b> wpływają na wzmocnienie u nasady, przyspieszenie wzrostu i zapobieganie przedwczesnemu wypadaniu. Naturalne ekstrakty roślinne powodują wygładzenie włosów sprawiając, że stają się one sprężyste i lśniące. Kliniczna formuła Keratin Hair Complex i najnowsze technologie pozwoliły na stworzenie produktu, którego skład decyduje o nawilżeniu i&nbsp;odżywieniu włosów, czyniąc je mniej łamliwymi i ograniczając wypadanie.
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <div class="hair3">
        <div class="hair3-line">
        </div>
        <div class="hair3-content">
            <div class="hair3-box">
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-ill.png" title="Włos chory" alt="Włos chory" />
                    </div>
                    <div class="hair3-box-text">
                        Włos suchy<br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-healthy.png" title="Włos zdrowy" alt="Włos zdrowy" />
                    </div>
                    <div class="hair3-box-text">
                        Włos zdrowy<br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-hair.png" title="DuoLife Keratin" alt="DuoLife Keratin" />
                    </div>
                    <div class="hair3-box-text">
                        <b>DuoLife Keratin<br>Hair Complex</b>
                    </div>
                </div>

            </div>
    </div>


<?php @include('inc/footer.php'); ?>
</body>
</html>