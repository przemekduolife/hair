<?php
$lang_menu="pl";
$subpage="sklad";
$title="Keratin Hair Complex-Skład";
$background="inherit";
@include('inc/header.php');
?>

<body>
<div id="composition1">
    <?php @include('inc/top.php');?>
    <div class="composition1-content">

        <h3>Kompleksowe działanie w&nbsp;3&nbsp;krokach</h3>

        <div class="composition1-box">
            <div class="composition1-icon-box">
                <a href ="#component2-content">
                    <div class="composition1-button-step">
                         Krok 1
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    pomaga włosom rosnąć
                </div>
            </div>
            <div class="composition1-icon-box">
                <a href ="#component3-content">
                    <div class="composition1-button-step">
                        Krok 2
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    hamuje wypadanie włosów
                </div>
            </div>
            <div class="composition1-icon-box">
                <a href ="#component4-content">
                    <div class="composition1-button-step">
                        Krok 3
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    dodaje włosom gęstości i&nbsp;objętości
                </div>
            </div>
        </div>
    </div>
    <div id="component2-content">

            <div class="component2-menu">
                <div class="component2-step1">
                    Krok 1
                </div>
                <div class="component2-break">
                </div>
                <a href="#component3-content">
                    <div class="component2-step2">
                        Krok 2
                    </div>
                </a>
                <div class="component2-break"></div>
                <a href="#component4-content">
                    <div class="component2-step3">
                        Krok 3
                    </div>
                </a>
            </div>
        <div class="component2-boxes-img">
            <!--<div class="component2-img">
                 <img  src="img/component-step1.png" title="Keratin Hair Complex pomaga włosom rosnąć" alt="hair" />
            </div>-->
        </div>
        <div class="component2-boxes">
            <div class="component2-text">
                <h5>
                    <b>Keratin Hair Complex</b> pomaga włosom rosnąć
                </h5>
                <div>
                    Ekstrakt z czarnego ryżu i opuncji figowej wpływają na:
                </div>
                <ul>
                    <li>
                        wzmocnienie zdolności komórek do walki ze stresem oksydacyjnym i ochronę przed skutkami zaburzeń hormonalnych,
                    </li>
                    <li>
                        pobudzanie włosów do wzrostu poprzez indukcję do namnażania komórek brodawkowatych występujących w mieszkach włosowych. Efektem są tzw."baby hair". Czyli widoczne, młode włoski, które zaczynają rosnąć. Najczęściej porastają wyższe partię czoła, choć mogą uwidocznić się również w okolicy szyi. Przyczyniają się do unoszenia włosów u nasady. Nowopowstałe włoski są sprężyste, wytrzymałe i wyglądają zdrowo.
                    </li>
                    <li>
                        ochronę włosów przed biologicznymi mechanizmami odpowiedzialnymi za ich wypadanie, dzięki właściwościom czarnego ryżu i mieszkanki ekstraktu z kwiatów opuncji poprzez połączenie efektu antyoksydacyjnego z hamowaniem 5-α-reduktazy,
                    </li>
                </ul>
                <div>
                    W składzie produktu znajduje się również ekstrakt z organicznych pędów grochu opracowany zgodnie z zasadami standaryzacji biologicznej w oparciu o technologię mikromacierzy DNA. Wpływa on na stymulację cząsteczek sygnalizujących w komórkach brodawek skórnych, decydujących o wzroście nowego włosa:
                </div>
                <ul>
                    <li>
                        Białko Noggin, białko skracające fazę telogenu
                    </li>
                    <li>
                        FGF7, czynnik wzrostu fibroblastów 7, który promuje rozpowszechnianie macierzy keratynocytów na początku nowej fazy anagenowej.
                    </li>
                </ul>
            </div>
        </div>
        <div class="component2-boxes2">
             <div class="component2-text2">
                <div class="component2-phrase-first">
                    <div class="component2-icon-info glyphicon glyphicon-info-sign"></div>
                    Analiza ekspresji genów została przeprowadzona na wyrwanych cebulkach włosów po dwutygodniowej kuracji z zastosowaniem surowca. Wyniki badania wykazały 56% wyższą regulację pozytywną Noggin i 85% wyższą regulację FGF-7. Wyniki te pokazują, że ekstrakt z organicznych pędów grochu zastosowany w Keratin Hair Complex jest w stanie stymulować brodawki skórne do wywoływania wzrostu nowych włosów.
                </div>
                <ul>
                    <li>
                        znacznego zmniejszenia utraty włosów po miesiącu (-34%),
                    </li>
                    <li>
                        znacznego zmniejszenia utraty włosów po dwóch miesiącach (-37%),
                    </li>
                    <li>
                        zwiększonej gęstości włosów,
                    </li>
                    <li>
                        znacznej redukcji wypadania włosów (86%),
                    </li>
                    <li>
                        76% ochotników wykazało chęć kontynuowania stosowania surowca.
                    </li>
                </ul>
                <div>
                    Grupa ochotników zażywała dzienną dawkę 100 mg produktu <b>– taką samą, jaką zastosowano w formulacji produktu DuoLife.</b>
                </div>
                <ul>
                    <li>
                        Naturalny produkt wspomagający wzrost włosów przygotowany z jadalnych, organicznych kiełków grochu (Pisum sativum)
                    </li>
                    <li>
                        Produkt opracowany zgodnie z zasadami standaryzacji biologicznej: lepsza ekspresja FGF7 i Nogginy równoważąca cykl wzrostu włosa
                    </li>
                    <li>
                        Kliniczna skuteczność udowodniona w przypadku zastosowania doustnego i aplikacji miejscowej u osób obu płci
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="component3-content">
        <div class="component3-menu">
            <a href="#component2-content">
                <div class="component3-step1">
                    Krok 1
                </div>
            </a>
            <div class="component3-break1">
            </div>
            <div class="component3-step2">
                Krok 2
            </div>
            <div class="component3-break2"></div>
            <a href="#component4-content">
                <div class="component3-step3">
                    Krok 3
                </div>
            </a>
        </div>
        <div class="component3-boxes-img" style="border-bottom: 0px;">
            <!--<div class="component3-img">
                <img  src="img/component-step2.png" title="Keratin Hair Complex hamuje wypadanie włosów" alt="hair" />
            </div>-->
        </div>
        <div class="component3-boxes">
            <div class="component3-text">
                <h5>
                    Keratin Hair Complex hamuje wypadanie włosów
                </h5>
                <div>
                    Składniki użyte w Keratin Hair Complex to kompleksowa odpowiedź na problem wypadających, słabych, cienkich i przerzedzających się włosów. Unikalna kompozycja składnikowa została dobrana tak, aby zoptymalizować proces stymulacji i odbudowy włosa. Wszystkie użyte składniki uzupełniają się wzajemnie, tworząc płynną Keratynową Terapię Odżywczą Włosa. Suplement diety zapobiega nadmiernemu wypadaniu włosów. Użyte składniki, takie jak ekstrakt z czarnego ryżu i opuncji figowej, ekstrakt z organicznych pędów grochu czy keratyna solubilizowana, zostały dobrane tak, aby kompleksowo wspomagać walkę z wypadaniem włosów. Ich wielokierunkowe działanie ponadto wzmacnia i poprawia ich kondycję. Zawarte w produkcie składniki posiadają kliniczną skuteczność*.
                </div>
                <div class="component3-przypis">
                    * dotyczy wybranych składników
                </div>
            </div>
        </div>
    </div>
    <div id="component4-content">
        <div class="component4-menu">
            <a href="#component2-content">
                <div class="component4-step1">
                    Krok 1
                </div>
            </a>
            <div class="component4-break1">
            </div>
            <a href="#component3-content">
                <div class="component4-step2">
                    Krok 2
                </div>
            </a>
            <div class="component4-break2"></div>
            <div class="component4-step3">
                Krok 3
            </div>
        </div>
        <div class="component4-boxes-img" style="border-bottom: 0px;">
            <!--<div class="component4-img">
                <img  src="img/component-step3.png" title="Keratin Hair Complex dodaje włosom gęstości i objętości" alt="hair" />
            </div>-->
        </div>
        <div class="component4-boxes">
            <div class="component4-text">
                <h5>
                    Keratin Hair Complex dodaje włosom gęstości i objętości
                </h5>
                <div>
                    Włosy potrzebują odpowiedniej ilości przyswajalnej siarki, która zawarta jest w krzemionce. Bogatym jej źródłem są skrzyp polny oraz pokrzywa. Systematyczne stosowanie produktów z ich zawartością pozwala widocznie poprawić stan włosów. Można liczyć na ograniczenie wypadania, wzmocnienie, odżywienie i regenerację.
                </div>
                <div>
                    W produkcie <b>Keratin Hair Complex</b> występują owoce amli, które są źródłem ważnej dla włosów witaminy C. Jej rolą jest zapewnienie szczelności naczyniom krwionośnym, stymulacja przepływu krwi w organizmie, co prowadzi do efektywnego transportu substancji odżywczych do włosów. Niedobór witaminy C przyczynia się do pogorszenia funkcjonowania skóry głowy, co sprawia, że proces tworzenia łodygi włosa zostaje zaburzony i pojawia się problem z wypadaniem włosów. Ten stan jest także podyktowany niedoborami żelaza w organizmie, a jego poziom systematycznie spada, gdy w organizmie występuje zbyt mało witaminy C.
                </div>
                <div>
                    Witamina C to kolejna funkcja, jaką jest produkcja kolagenu – podstawowego budulca włosów odpowiedzialnego za ich elastyczność i sprężystość.
                </div>
                <div>
                    Zawarty w produkcie ekstrakt z pestek winogron, które stanowią bogate żródło aktywnych biologicznie oligomerycznych proantocyjanidyn (OPC - oligomericproanthocyanidincomplexes), wpływa na odżywianie i stymulację cebulek włosów. Składniki te decydują o ulepszonej strukturze włosa i uszczelnieniu jego warstwy ochronnej.
                </div>
            </div>
        </div>
    </div>
<div class="composition3">
    <div class="composition3-content">

        <h3>
            Zobacz jak działa<br>DuoLife Keratin Hair Complex
        </h3>
        <div><a href="https://www.youtube.com/watch?v=CbOn8YwRDH0" target="_blank">
                <img  src="img/play-now-icon.svg" title="Zobacz" alt="Zobacz" />
            </a>
        </div>
        <div class="composition-bottle-left">
            <img  src="img/composition-bottle-left.png" title="Keratin Hair Complex" alt="Keratin Hair Complex-bottle" />
        </div>
        <div class="composition-bottle-right">
            <img  src="img/composition-bottle-right.png" title="Keratin Hair Complex" alt="Keratin Hair Complex-bottle" />
        </div>

    </div>
</div>
<div class="composition4">
    <div class="composition4-content">
        <div class="composition4-kropla">
            <!--<img src="img/kropla.svg">-->
        </div>
        <div class="composition4-left-text">
            <h3>Zweryfikuj efekty<br>
                Twojej 3-miesięcznej<br>
                kuracji
            </h3>
            <div>
                Przesuń linię, aby zobaczyć jak DuoLife Keratin Hair Complex
                wpływa na wygląd włosów po 3-miesięcznej kuracji.
            </div>
        </div>
        <div class="wrapper">
            <div class="before">
                <img class="content-image" src="img/hair-compare-new4.png" draggable="false"/>
                <div class="after">
                    <img class="content-image" src="img/hair-compare-old4.png" draggable="false"/>
                </div>
            </div>
            <div class="scroller">
                <svg class="scroller__thumb" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100"><polygon points="0 50 37 68 37 32 0 50" style="fill:#fff"/><polygon points="100 50 64 32 64 68 100 50" style="fill:#fff"/></svg>
            </div>
        </div>
    </div>
</div>
<div class="composition5">
    <div class="composition5-content">
        <div class="composition5-boxes-img" style="border-bottom: 0px;">
            <!--<div class="composition5-img">
                <img  src="img/composition5-img.png" title="Kompleksowo, czyli nie tylko Terapia Włosa" alt="Kompleksowo, czyli nie tylko Terapia Włosa" />
            </div>-->
        </div>
        <div class="composition5-boxes">
            <div class="composition5-text">
                <h5>
                    Kompleksowo, czyli nie tylko Terapia Włosa
                </h5>
                <div>
                    Warto podkreślić, że produkt działa dobroczynnie nie tylko na włosy, lecz również na skórę i paznokcie.
                </div>
                <div>
                    Dzięki wyciągowi z czarnego ryżu i opuncji figowej Keratin Hair Complex wykazuje synergistyczne działanie przeciwko trądzikowi poprzez wzmocnienie zdolności komórek do walki ze stresem oksydacyjnym oraz ochronę przed skutkami zaburzeń hormonalnych. Przeciwdziała efektom wywołanym działaniem testosteronu w sebocytach skóry twarzy. Połączenie efektu antyoksydacyjnego z hamowaniem 5-α-reduktazy powoduje synergistyczne działanie ochronne czarnego ryżu i mieszanki ekstraktu z kwiatów opuncji przeciwko mechanizmom biologicznym odpowiedzialnym za powstawanie trądziku. Solubilizowana keratyna to rewolucyjny składnik nutrikosmeceutyczny. Surowiec uzupełnia organizm o biodostępną keratynę, która pomaga w ochronie i naprawie uszkodzonych tkanek paznokci oraz skóry.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="composition6">
    <div class="composition6-content">
        <div class="composition6-boxes">
            <div class="composition6-text">
                <div class="composition6-text-skladniki">
                    <b>Składniki:</b> puree z owoców gujawy, sok z owoców malin, formuła ekstraktu z czarnego ryżu i kwiatów opuncji figowej standaryzowana na zawartość antocyjanów (250mg/50ml), ekstrakt z owoców amli standaryzowany na zawartość witaminy C, ekstrakt z organicznych pędów grochu (100mg/50ml), keratyna solubilizowana (100mg/50ml), ekstrakt z liści pokrzywy zwyczajnej (70mg/50ml), ekstrakt z pestek winogron standaryzowany na zawartość polifenoli (50mg/50ml), ekstrakt z ziela skrzypu polnego (50mg/50ml).
                </div>
                <div class="composition6-table">
                    <table cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <th>Składniki</th>
                                <th>25 ml</th>
                                <th>50 ml</th>
                                <th>100 ml</th>
                            </tr>
                            <tr>
                                <td>Witamina C</td>
                                <td>50 mg</td>
                                <td>100 mg</td>
                                <td>200 mg</td>
                            </tr>
                            <tr>
                                <td>RWS*</td>
                                <td>63%</td>
                                <td>125%</td>
                                <td>250%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="composition6-przypis">
                    *RWS - Referencyjnawartość spożycia dla przeciętnej osoby dorosłej (8400kJ/2000kcal)
                </div>
                <div>
                    <b>Objętość:</b> 750ml
                </div>
                <div>
                    Dawkownie: 25ml do 50ml raz dziennie przed posiłkiem
                </div>
            </div>
        </div>
    </div>
</div>
<?php @include('inc/footer.php'); ?>
</body>
</html>