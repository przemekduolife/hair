<?php
$lang_menu="pl";
$subpage="kontakt";
$title="Keratin Hair Complex - Kontakt";
$background="#ffffff";
@include('inc/header.php');
?>
<body>
<div class="contact">
    <?php @include('inc/top.php'); ?>
    <div class="contact-content">
        <div class="contact-boxes">
            <div class="contact-box">
                <h3>Kontakt</h3>
                <div class="contact-text">
                    <span>Wyprodukowano w UE dla:</span><br><br>
                    <b>DuoLife S.A.</b><br>
                    ul. Topolowa 22,<br>
                    Więckowice k/Krakowa<br><br>
                    Infolinia: +48 12 333 45 67<br>
                    info@duolife.eu
                </div>
                <div class="contact-button">
                    <a title="odwiedź stronę prodcenta"   href="https://myduolife.com" target="_blank">
                        <div class="contact-button-white">
                            odwiedź stronę prodcenta
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="contact-img">
            <img  src="img/kontakt-bottles.png" title="Keratin Hair Complex" alt="Keratin Hair Complex - bottles" />
        </div>
    </div>
</div>
<?php @include('inc/footer.php'); ?>
</body>
</html>