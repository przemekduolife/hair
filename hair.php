<?php
$lang_menu="pl";
$subpage="hair";
$title="Keratin Hair Complex - Hair";
$background="inherit";
@include('inc/header.php');
?>

<body>
<div id="desc1">
    <?php @include('inc/top.php'); ?>
    <div class="desc1-content">
        <div class="desc1-boxes">
            <div class="desc1-box">
                <h3>Wzrost włosów – <br>kropla wiedzy o...</h3>
                <div class="desc1-text">
                    <b>100-150 tysięcy</b> - to liczba włosów na głowie człowieka.  Czy wiesz, że na 1 cm2 występuje od 150 do 500 włosów ciemnych i od 180 do 750 włosów jasnych? Wystarczy 1 dzień, by na głowie włosy urosły o 0,35 mm. W ciągu miesiąca przyrost wynosi około 1 cm, co daje nawet 12 cm rocznie. Mówimy około, bowiem na wzrost włosów mają wpływ czynniki zewnętrzne. W tym samym okresie – jednego roku, z głowy człowieka może wypaść nawet 35 tysięcy włosów.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc2">
    <div class="desc2-content">
        <div class="desc2-boxes">
            <div class="desc8-img">
                <img src="img/hair-etapy-img.png" title="Powszechny problem wypadania włosów" alt="Powszechny problem wypadania włosów">
            </div>
            <div class="desc2-box">
                <div class="desc2-box1">
                    <span>Anagen</span>
                    <span>Katagen</span>
                    <span>Telogen</span>
                </div>
                <div class="desc2-text">
                    Wzrost włosów odbywa się w trzech fazach. Cały cykl charakteryzuje się czasem asynchronicznym, co oznacza, że nowe włosy pojawiają się na głowie w regularnych odstępach czasu i nie dochodzi do okresowego łysienia.
                </div>
                <div class="desc2-box3">
                    <div class="desc2-box3-left">
                        <div class="step1"></div>
                        <div class="desc2-left-text phase1">
                            Pierwsza faza wzrostu włosów to anagen. Etap najważniejszy, ponieważ jest to faza aktywna. W niej zaś znajduje się aż 85-90% włosów. W anagenie włos trwale łączy się z mieszkiem włosowym. Zostaje odżywiony i przyrasta na długość. W zależności od uwarunkowań genetycznych i trybu życia, u zdrowej osoby wzrost włosów na głowie trwa średnio 6 lat.
                        </div>
                    </div>
                    <div class="desc2-box3-right">
                        <div class="step2"></div>
                        <div class="desc2-right-text phase2">
                            Druga faza to katagen, charakteryzujący się obumieraniem włosów, co polega na odłączeniu się mieszka włosowego. Stopniowe obkurczanie cebulki włosowej prowadzi do wysuwania włosa z mieszka włosowego. Ta faza średnio trwa 3 tygodnie, ale znajduje się w niej zaledwie 1-3% włosów.
                        </div>
                    </div>
                    <div class="desc2-box3-left">
                        <div class="step3"></div>
                        <div class="desc2-left-text phase3">
                            Ostatnia faza – spoczynku mieszka włosowego – to telogen, w którym znajduje się około 8-15% włosów. W tym czasie włosy nie rosną, a mieszek włosowy pozostaje pusty. Telogen trwa nawet kilka miesięcy.
                        </div>
                    </div>
                </div>
                <div class="desc2-text">
                Rozwój mieszków włosowych ma swój początek w życiu płodowym. Około 60 dnia życia zaczynają się tworzyć mieszki włosowe, ale cały cykl wzrostu włosa, zależny jest od wielu czynników. To dlatego powszechnie uważa się, że stan włosów odzwierciedla kondycję organizmu.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc3">
    <div class="desc3-content">
        <div class="desc3-boxes">
            <div class="desc3-box">
                <h5>Skład włosa</h5>
                <div class="desc8-img">
                    <img src="img/desc3-composition.png" title="Powszechny problem wypadania włosów" alt="Powszechny problem wypadania włosów">
                </div>
                <div class="desc3-box-composition">
                    <div class="comp1">
                        Keratyna 80%
                    </div>
                    <div class="comp1-height"></div>
                    <div class="comp2">
                        Woda 10%
                    </div>
                    <div class="comp2-height"></div>
                    <div class="comp3">
                        Tłuszcze 5.8%
                    </div>
                    <div class="comp3-height"></div>
                    <div class="comp4">
                        Cukry, aminokwasy i sole mineralne 4,2%
                    </div>
                    <div class="comp4-height"></div>

                </div>
                <div class="desc3-box-text">
                    <div>
                        Około 80% każdego włosa stanowią włókna keratynowe będące podstawą ich składu. Jest to białko zbudowane z aminokwasów połączonych włóknami peptydowymi. Keratyna we włosie może mieć postać eukeratyny, która charakteryzuje się elastycznością, sprężystością i dużą rozciągliwością i jest najbardziej pożądana spośród wszystkich. Druga postać do pseudokeratyna występująca w wyciągniętych włóknach keratyny.
                    </div>
                    <div>
                        Proces keratynizacji polega na wymianie obumarłych komórek na żywe, w których zawiera się keratyna. Dzięki niemu skóra, ale i włosy oraz paznokcie przez cały czas podlegają zamianie z obumarłych na żywe, zawierające keratynę. Powstające kertynocyty spełniają funkcje ochronne na skórę, włosy i paznokcie. Skutkiem tych działań jest młody, świeży i zadbany wygląd.
                    </div>
                    <div>
                        Włókna keratynowe tworzące keratynę są powiązane ze sobą wiązaniami siarczkowymi, co wpływa na elastyczność i sprężystość włosów oraz skóry. Keratyna obecna w strukturze włosa nie rozpuszcza się w wodze, ale niestety podatna jest na działanie wysokiej temperatury. Łatwo ją zniszczyć, np. przegrzewając włosy prostownicą, suszarką, lokówką czy też stosując niewłaściwe preparaty.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc4">
    <div class="desc4-content">
        <div class="desc4-boxes">
            <div class="desc4-text-h5">
                <h5>Wygląd i struktura włosów</h5>
            </div>
            <div class="hair3-box">
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-ill.png" title="Włos chory" alt="Włos chory" />
                    </div>
                    <div class="hair3-box-text">
                        Włos chory<br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-healthy.png" title="Włos zdrowy" alt="Włos zdrowy" />
                    </div>
                    <div class="hair3-box-text">
                        Włos zdrowy<br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="img/hair3-img-hair.png" title="DuoLife Keratin" alt="DuoLife Keratin" />
                    </div>
                    <div class="hair3-box-text">
                        <b>DuoLife Keratin<br>Hair Complex</b>
                    </div>
                </div>

            </div>
            <div class="desc4-text">
                <h5>
                    Skład jakościowo-ilościowy produktu DuoLife Keartin Hair Complex to odwzorowanie składu włosa
                </h5>
                <div>
                    Melanina, pigment wytwarzany przez melanocyty i znajdujący się w macierzy włosa, decyduje o ich kolorze. Im większa jej ilość, tym ciemniejsze są włosy. Melanina występuje w dwóch postaciach.
                </div>
                <div>
                    Eumelanina sprawia, że nasze włosy są ciemne lub jasne – czyli odpowiada za barwę włosów.
                </div>
                <div>
                    Feomelanina odpowiada za odcień włosów – czyli czy jest za ciepły czy chłodny.
                </div>
                <div>
                    Jak to się dzieje, że po wakacjach w słonecznych klimatach nasze włosy jaśnieją? Sprawcą jest ponownie melanina, która jako białko wykazuje wrażliwość na działanie czynników środowiskowych oraz środków chemicznych.
                </div>
                <div>
                    A co poza tym?
                </div>
                <div>
                    W składzie włosów znajduje się również woda, której zawartość wynosi około 10%. Jej obecność jest bardzo istotna, gdyż tylko dobrze nawilżone włosy wyglądają zdrowo. Ponadto pozostałe składniki to: tłuszcze stanowiące 5,8%. Do tego cukry, aminokwasy i sole mineralne.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc5">
    <div class="desc5-content">
        <div class="desc5-boxes">
            <div class="desc5-text">
                <div class="desc5-title">
                    <div class="desc5-icon-info glyphicon glyphicon-info-sign"></div>
                    <h5>Struktura włosa warunkowana przez geny</h5>
                </div>

                <div>
                    Kod genetyczny decyduje nie tylko o naszym zdrowiu, ale i wyglądzie włosów. Cecha w postaci prostych lub kręconych włosów, powstaje ze względu na obecność genu nazwanego trichohyalinem. Jeśli jego równowaga aminokwasowa ulega zaburzeniu, włosy zaczynają się kręcić.
                </div>
                <div>
                    Warto przy tym podkreślić, że rodzaj włosów to cecha, która może zostać przekazana nie tylko w linii prostej. Oznacza to, że dziecko może mieć kręcone włosy nawet wtedy, gdy oboje rodziców mają włosy proste.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc6">
    <div class="desc6-content">
        <div class="desc6-boxes">
            <h5>
                Przekrój poprzeczny włosa ma znaczenie
            </h5>
            <div class="desc6-box">
                <div class="desc6-box-comp">
                    <p class="desc6-comp1">warstwa rdzenna</p>
                    <div class="desc6-index1"><span class="index-number">1</span></div>
                    <div class="desc6-line1"></div>
                    <div class="desc6-index1-square"><span class="index-number">1</span></div>
                    <p class="desc6-comp2">warstwa korowa</p>
                    <div class="desc6-index2"><span class="index-number">2</span></div>
                    <div class="desc6-line2"></div>
                    <div class="desc6-index2-square"><span class="index-number">2</span></div>
                    <p class="desc6-comp3">powłoczka włosa</p>
                    <div class="desc6-index3"><span class="index-number">3</span></div>
                    <div class="desc6-line3"></div>
                    <div class="desc6-index3-square"><span class="index-number">3</span></div>
                    <p class="desc6-comp4">osłonka pochewki wewnętrznej</p>
                    <div class="desc6-index4"><span class="index-number">4</span></div>
                    <div class="desc6-line4"></div>
                    <div class="desc6-index4-square"><span class="index-number">4</span></div>
                    <p class="desc6-comp5">warstwa nabłonkowa ziarnista (Huxleya)</p>
                    <div class="desc6-index5"><span class="index-number">5</span></div>
                    <div class="desc6-line5"></div>
                    <div class="desc6-index5-square"><span class="index-number">5</span></div>
                    <p class="desc6-comp6">warstwa nabłonkowa jasna (Henlego)</p>
                    <div class="desc6-index6"><span class="index-number">6</span></div>
                    <div class="desc6-line6"></div>
                    <div class="desc6-index6-square"><span class="index-number">6</span></div>
                    <p class="desc6-comp7">pochewka zewnętrzna</p>
                    <div class="desc6-index7"><span class="index-number">7</span></div>
                    <div class="desc6-line7"></div>
                    <div class="desc6-index7-square"><span class="index-number">7</span></div>
                    <p class="desc6-comp8">brodawka włosa</p>
                    <div class="desc6-index8"><span class="index-number">8</span></div>
                    <div class="desc6-line8"></div>
                    <div class="desc6-index8-square"><span class="index-number">8</span></div>
                    <p class="desc6-comp9">cebulka włosa</p>
                    <div class="desc6-index9"><span class="index-number">9</span></div>
                    <div class="desc6-line9"></div>
                    <div class="desc6-index9-square"><span class="index-number">9</span></div>
                </div>
            </div>
            <div class="desc6-text">
                <div>
                    Kod genetyczny decyduje nie tylko o naszym zdrowiu, ale i wyglądzie włosów. Cecha w postaci prostych lub kręconych włosów, powstaje ze względu na obecność genu nazwanego trichohyalinem. Jeśli jego równowaga aminokwasowa ulega zaburzeniu, włosy zaczynają się kręcić.
                </div>
                <div>
                    Warto przy tym podkreślić, że rodzaj włosów to cecha, która może zostać przekazana nie tylko w linii prostej. Oznacza to, że dziecko może mieć kręcone włosy nawet wtedy, gdy oboje rodziców mają włosy proste.
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc7">
    <div class="desc7-content">
        <div class="desc7-boxes">
            <div class="desc7-box">
                <h5>
                    Czy wiesz, jakie jest skręt Twojego włosa?
                </h5>
                <div class="desc7-text">
                    Odpowiednia pielęgnacja włosów jest bardzo ważna, bo pozwala zadbać o ich kondycję i sprawić, że będą zawsze pięknie się prezentowały. Naocznie widzimy, jak proste lub poskręcane są nasze włosy, ale warto stopień skrętu sprawdzić dokładnie, a jest na to prosty sposób.
                </div>
                <div class="desc7-exp">
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">1</span></div>
                            <img  src="img/experiment-001.png" title="experiment1" alt="experiment1" />
                        </div>
                        <div class="desc7-exp-text">
                            Narysuj kółko na kartce papieru o średnicy np. dna kubka,
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">2</span></div>
                            <img  src="img/experiment-002.png" title="experiment2" alt="experiment2" />
                        </div>
                        <div class="desc7-exp-text">
                            Wyrwij około 15 cm włos ze środka głowy,
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">3</span></div>
                            <img  src="img/experiment-003.png" title="experiment3" alt="experiment3" />
                        </div>
                        <div class="desc7-exp-text">
                            Zamocz włos w wodzie i po wyjęciu odczekaj<br> aż całkowicie wyschnie
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">4</span></div>
                            <img  src="img/experiment-004.png" title="experiment4" alt="experiment4" />
                        </div>
                        <div class="desc7-exp-text">
                            Ułóż go na narysowanym obrysie kółka.
                        </div>
                    </div>
                </div>
                <h5>
                    Wyniki:
                </h5>
                <div class="desc7-text">
                    <div>
                        1. Twoje włosy możesz zaliczyć do prostych, jeśli w obrysie mieści się połowa włosa,
                    </div>
                    <div>
                        2. Jeśli w obrysie kółka zmieściło się przynajmniej 3/4 włosa – Twoje kosmki zalicza się do kręconych.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc8">
    <div class="desc8-content">
        <div class="desc8-boxes">
            <div class="desc8-img">
                <img src="img/desc8-image.png" title="Powszechny problem wypadania włosów" alt="Powszechny problem wypadania włosów">
            </div>
            <div class="desc8-box">
                <h5>
                    Powszechny problem wypadania włosów
                </h5>
                <div class="desc8-text">
                    Opisane uprzednio fazy cyklu wzrostu włosa i ich ilość przypadająca na każdą z nich, są charakterystyczne dla osób zdrowych. U ludzi o zdrowych włosach około 85-90% jest w fazie anagenowej, a pozostałe 10-15% znajduje się w fazie telogenowej. Gdy dochodzi do zaburzeń w równowadze cyklu wzrostu włosów, wzmaga się wypadanie włosów i może dojść do stopniowego łysienia. Przyczyną takiego stanu mogą być:
                </div>
                <div class="desc8-list">
                    <div class="desc8-list-left">
                        <ul>
                            <li>permanentny stres,</li>
                            <li>nieprawidłowa dieta,</li>
                            <li>zaburzenia hormonalne,</li>
                        </ul>
                    </div>
                    <div class="desc8-list-right">
                        <ul>
                            <li>niewłaściwa pielęgnacja,</li>
                            <li>genetyka.</li>
                        </ul>
                    </div>
                </div>
                <h5>
                    Co dzieje się z włosami o zaburzonym cyklu wzrostu?
                </h5>
                <div class="desc8-text">
                    <div>
                        1. Proporcje włosów anagenowych i telogenowych ulegają zaburzeniu,
                     </div>
                    <div>
                        2. Pojawia się coraz mniej włosów anagenowych, a systematycznie przybywa tych w fazie telogenu,
                    </div>
                    <div>
                        3. Skraca się czas trwania fazy anagenowej, co owocuje pojawianiem się włosów krótszych i cieńszych,
                    </div>
                    <div>
                        4. Pojawiają się zastępcze włosy anagenowe, w skutek przedłużenia odstępu oddzielającego stratę a włos w fazie telogenowej
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php @include('inc/footer.php'); ?>
</body>
</html>