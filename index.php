<?php
$lang_menu="pl";
$subpage="main";
 $title="Keratin Hair-Complex - Strona Główna";
  @include('inc/header.php');
 ?>

<body>
	<div id="main">
        <?php @include('inc/top.php'); ?>
		<div class="main1-content">
            <div class="main1-img">
                <img  src="img/main1-bottle.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
            </div>
            <div class="main1-text">
                <h3>
                   DuoLife Keratin<br>
                   Hair Complex
                </h3>
                <h4>
                    Keratin Therapy. For your internal strength
                </h4>
                <div>
                    <b>DuoLife Keratin Hair Complex</b> to kontynuacja produktów płynnych. To suplement diety w formie płynnej na bazie keratyny, zawierający naturalnie składniki pochodzenia roślinnego. Są one opatrzone  międzynarodowymi patentami*. W 100% naturalne, przebadane klinicznie*. Ich skuteczność została potwierdzona badaniami dotyczącymi wzrostu i kondycji włosa. Surowce użyte w procesie formulacji mają udowodnioną skuteczność w przypadku zastosowania doustnego i przeznaczony jest do stosowania przez kobiety i mężczyzn*.
                </div><br>
                <div class="main1-przypis">
                    * dotyczy wybranych składników
                </div>
            </div>

        </div>
        <div class="main2-content">
            <div class="main2-boxes">
                <div class="main2-box">
                    <div class="main2-text">
                        <b>DuoLife Keratin Hair Complex zdrowie i piękno w jednym</b>
                    </div>
                    <div class="main2-icon-boxes">
                        <div class="main2-icon-box">
                            <div>
                                <img  src="img/main2-34percent.png" title="34%" alt="34%" />
                            </div>
                            <div class="main2-box-text">
                                <div>Wypadanie Twoich włosów zmniejszone o 34% już po miesiącu stosowania*</div>
                            </div>
                        </div>
                        <div class="main2-icon-box">
                            <div>
                                <img  src="img/main2-86percent.png" title="86%" alt="86%" />
                            </div>
                            <div class="main2-box-text">
                                <div>Znaczna redukcja wypadania<br>
                                    włosów o 86% po pełnej kuracji*</div>
                            </div>
                        </div>
                        <div class="main2-icon-box">
                            <div>
                                <img  src="img/main2-icon10.png" title="10x/2x" alt="10x/2x" />
                            </div>
                            <div class="main2-box-text">
                                <div>Twoje włosy są 10x mocniejsze i 2x bardziej sprężyste, niż przed terapią*</div>
                            </div>
                        </div>
                        <div class="main2-icon-box main2-icon4">
                            <div>
                                <img  src="img/main2-iconXXL.png" title="xxl" alt="xxl" />
                            </div>
                            <div class="main2-box-text">
                                <div>Znaczna poprawa objętości i gęstości XXL dla włosów cienkich*</div>
                            </div>
                        </div>
                        <div class="main2-icon-box main2-icon5">
                            <div>
                                <img  src="img/main2-icon3.png"  title="3" alt="3" />
                            </div>
                            <div class="main2-box-text">
                                <div>Rezultaty działania widoczne <br>
                                    po 3 miesiącach stosowania*</div>
                            </div>
                        </div>
                    </div>
                    <div class="main2-przypis">
                        * Badania przeprowadzone na grupie 21 osób. Weryfikacja działania sprawdzana co 2 tygodnie stosowania.<br>Badanie wykonane w labolatorium badawczym.
                    </div>
                </div>
            </div>
		</div>
    </div>
    <div class="main3-content">
        <div class="main3-boxes">
            <div class="main3-box">
                <div class="main3-text">
                    Poznaj zalety DuoLife Keratin Hair Complex
                </div>
                <div class="main3-icon-boxes">
                    <div class="main3-icon-box">
                        <div>
                            <img  src="img/icon1-keratin.png" title="Liquid Keratin" alt="Liquid Keratin" />
                        </div>
                        <div class="main3-box-text">
                           zawiera solubilizowaną, <br>płynną kreatynę
                        </div>
                    </div>
                    <div class="main3-icon-box">
                        <div>
                            <img  src="img/icon2-keratin.png" title="kliniczna skuteczność" alt="kliniczna skuteczność" />
                        </div>
                        <div class="main3-box-text">
                            kliniczna skuteczność*
                        </div>
                    </div>
                    <div class="main3-icon-box">
                        <div>
                            <img  src="img/icon3-keratin.png" title="międzynarodowe patenty" alt="międzynarodowe patenty" />
                        </div>
                        <div class="main3-box-text">
                            surowce posiadają międzynarodowe patenty*
                        </div>
                    </div>
                    <div class="main3-icon-box">
                        <div>
                            <img  src="img/icon4-keratin.png" title="Ant-Aging Complex" alt="Ant-Aging Complex" />
                        </div>
                        <div class="main3-box-text">
                            Ant-Aging Complex
                        </div>
                    </div>
                    <div class="main3-icon-box main3-icon5">
                        <div>
                            <img  src="img/icon5-keratin.png" title="Logo Prooptical" alt="icon" />
                        </div>
                        <div class="main3-box-text">
                            100% naturalny
                        </div>
                    </div>
                    <div class="main3-icon-box">
                        <div>
                            <img  src="img/icon6-keratin.png" title="nie zawiera substancji konserwujących" alt="nie zawiera substancji konserwujących" />
                        </div>
                        <div class="main3-box-text">
                            nie zawiera substancji<br>konserwujących
                        </div>
                    </div>
                     <div class="main3-icon-box main3-icon7">
                        <div>
                            <img  src="img/icon7-keratin.png" title="100% GMO free" alt="100% GMO free" />
                        </div>
                        <div class="main3-box-text">
                            nie zawiera produktów genetycznie<br> modyfikowanych
                        </div>
                     </div>
                </div>
                <div class="main3-przypis">
                    * Dotyczy wybranych składników.
                </div>
             </div>
        </div>
    </div>
    <div class="main4-content">
        <div class="main4-boxes">
            <div class="main4-box">
                <div class="main4-box-text">
                    <h4>
                        DuoLife Keratin Hair Complex zawiera<br>100%&nbsp;naturalnych składników
                    </h4>
                    <p>
                        puree z owoców gujawy
                    </p>
                    <div class="line-img"></div>
                    <p>
                        ekstrakt z owoców amli
                    </p>
                    <div class="line-img"></div>
                    <p>
                        keratyna solubilizowana
                    </p>
                    <div class="line-img"></div>
                    <p>
                        ekstrakt z pestek winogron
                    </p>
                    <div class="line-img"></div>
                    <p>
                        formuła ekstraktu z czarnego ryżu i kwiatów opuncji figowej
                    </p>
                    <div class="line-img"></div>
                    <p>
                        ekstrakt z organicznych pędów grochu
                    </p>
                    <div class="line-img"></div>
                    <p>
                        ekstrakt z liści pokrzywy zwyczajnej
                    </p>
                    <div class="line-img"></div>
                    <p>
                        ekstrakt z ziela skrzypu polnego
                    </p>
                    <div class="line-img"></div>
                    <p>
                        Sok z owoców z malin
                    </p>
                    <div class="line-img"></div>
                </div>
                <div class="main4-img">
                    <img  src="img/main4-droplet.png" title="drop" alt="drop" />
                </div>
                <div class="main4-img-resp">
                    <img  src="img/main4-droplet.png" style ="width: 281px; height: 448px" title="drop" alt="drop" />
                </div>
            </div>
        </div>
    </div>
    <div class="main5">
        <div class="main5-content">
            <div class="main5-boxes">
                <div class="main5-box">
                    <div class="main5-img">
                        <img  src="img/main5-bottle.png" title="Keratin Hair Complex" alt="Keratin Hair Complex" />
                    </div>
                    <div class="main5-box-text">
                        <h3>DuoLife Keratin<br>Hair Complex</h3>
                        <h4>100% solubilizowana,<br>płynna keratyna</h4>
                        <div class="main5-button-box">
                            <div class="main5-button-buy">
                                <a class="button-white" href="https://myduolife.com/shop/categories/1,1,produkty-duolife.html" target="_blank">Kup teraz</a>
                            </div>
                        </div>
                        <div class="main5-button-box">
                            <div class="main5-button-leaflet">
                                <a class="button-dark" href="ulotka-duolife-keratin-hair-complex.pdf" target="_blank">pobierz ulotkę</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?php @include('inc/footer.php'); ?>
</body>
</html>			