var ScrollHair = {
    active: false,
    scroller: document.querySelector('.scroller'),

    scrollIt: function(x) {
        var transform = Math.max(0, (Math.min(x, document.querySelector('.wrapper').offsetWidth)));
        document.querySelector('.after').style.width = transform + "px";
        this.scroller.style.left = transform - 25 + "px";
    },

    checkFlags: function(e) {
        if(ScrollHair.active === true) {
            ScrollHair.active = false;
            ScrollHair.scroller.classList.remove('scrolling');
        } else {
            ScrollHair.active = true;
            ScrollHair.scroller.classList.add('scrolling');
        }
    },

    handleScroll: function(e) {
        if (!ScrollHair.active) return;
        var x = e.pageX;
        x -= document.querySelector('.wrapper').getBoundingClientRect().left;
        ScrollHair.scrollIt(x);
    },

    init: function(width) {
        this.scroller.addEventListener('mousedown', this.checkFlags, false);
        this.scroller.addEventListener('touchstart', this.checkFlags, false);

        document.body.addEventListener('mouseup', this.checkFlags, false);
        document.body.addEventListener('touchend', this.checkFlags, false);

        document.body.addEventListener('mouseleave', this.checkFlags, false);
        document.body.addEventListener('touchcancel', this.checkFlags, false);

        document.body.addEventListener('mousemove', this.handleScroll, false);
        this.scrollIt(width);
    }
};

window.addEventListener('load', function () {
    var width = 420;
    ScrollHair.init(width);

    window.addEventListener('resize', function() {
        if (window.screen.width < 1200) {
            ScrollHair.init(360);
        } else {
            ScrollHair.init(width);
        }
    });
});