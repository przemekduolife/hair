$(document).ready(function() {
$(".zmianajezyka_menu").hide();
    
$("#content").click(function(){
    $(".zmianajezyka_menu").hide();
    $(".zmianajezyka_lista2").hide();
}); 

$(".baza-generator").click(function(){
    $(".zmianajezyka_menu").hide();
    $(".zmianajezyka_lista2").hide();
}); 



    
$(".zmianajezyka_linkaktywny").click(function(){
    $(".zmianajezyka_menu").toggle();
}); 
    
$(".zmianajezyka_lista2").hide();
    
$(".menujezykzmiana").click(function(){
    $(".zmianajezyka_lista2").toggle();
}); 
    
//rozwijanie i zwijanie menu gornym navie

$(".ddn>ul>li").hide();
    
$('.menu-rozwijane').click(function() {
    $(this).toggleClass('menu-rozwiniete');
    $(this).closest("li").find("li").slideToggle(1000);
   // $(this).closest("li").siblings(".ddn").find("li").slideUp(1000);
    $(this).closest(".box-disc_mobile").siblings().find(".momo").slideUp(1000);
});    
    
//rozwijanie duzego menu
    
$(".maps, .menu-button-close").hide();  
    
$(".ico-menu").click(function(){
    $(".menu-button-close, .menu-button-open").toggle();
    $(".maps").slideToggle(1000);
    $(".zmianajezyka_linkaktywny, .szukaj_ico").toggle();
    $(".main").toggleClass("malymain");
});

//rozwijane menu w bazie
    
$(".baza-choose-menu>ul").hide();
    
$(".baza-choose-menu").click(function(){
    $(this).find("ul").slideToggle({origin: ["bottom"]}, 500);
    $(this).find(".reorder-img").toggleClass("reorder-img-2");
});
    
//skrypty do baza-mend.php

$(".table-bme").hide();
    
$(".bme").click(function(){
   $(this).closest("div").find(".table-bme").slideToggle(400);
    $(this).toggleClass("bme-down");
});

// skrypty do baza-mend.php - tylko jezyk polski


    
$(".bme-pl").click(function(){
   $(this).closest("div").find(".table-bme-pl").slideToggle(400);
    $(this).toggleClass("bme-down-pl");
});

// obrazki

$(".baza_mend_obraz_full").hide(); 
    
$(".obraz360").click(function(){
    $(".baza_mend_obraz_full").slideDown(1000);
	
    $(".baza_mend_obraz").css("visibility", "hidden");
}); 
    
$(".baza_mend_obraz_full").click(function() {
	$(".baza_mend_obraz_full").slideUp(1000);
	$(".baza_mend_obraz").css("visibility", "visible");
});
    
$('.reorder-ul').hide();

$('.baza-choose-menu-button').click(function(){
    
   $(this).siblings('.reorder-ul').slideToggle(300); 
    $(this).toggleClass("k");
});

//skrypty do faq.php
$(".faq-container").hide();
    
$(".fq2").click(function(){
   $(".faq-container").slideDown(300);
    
});

$(".faq-close").click(function(){
   $(".faq-container, .faq-container-error, .error").slideUp(300);
    
});

$(".faq-answer").hide();
    
$(".faq-question").click(function(){
   $(this).closest(".faq-main").find(".faq-answer").slideToggle(400);
    $(this).toggleClass("bme-down");
});
    
//skrypty mobilne
    

var mobile = function() {
    if(Modernizr.mq("only screen and (max-width: 1200px)")) {
        $(".box-disc").find(".momo").hide();
        $(".maps-view").find("div").removeClass("box-disc");
        $(".maps-view").find("div").addClass("box-disc_mobile");
        $(".box-disc_mobile").find("p").addClass("box-disc_mobile_p");        
    } else {
        $(".box-disc_mobile").find("p").removeClass("box-disc_mobile_p");
        $(".maps-view").find("div").removeClass("box-disc_mobile");
        $(".maps-view").find("div").addClass("box-disc");
        $(".box-disc").find("ul").show();
    };
};

$(window).resize(mobile);

mobile();
    
//$('.box-disc_mobile_p').click(function() {
//    $(this).find("ul").slideToggle(1000);
 //   $(this).siblings().find("ul").slideUp(1000);
//});
    
$(".box-disc_mobile_p").click(function() {
    $(this).siblings(".momo").slideToggle(1000);
    $(this).closest(".box-disc_mobile").siblings().find(".momo").slideUp(1000);
    $(this).closest(".maps-view").find(".ddn").find("li").slideUp(1000);
    $(".menu-rozwijane").removeClass("menu-rozwiniete");
});

$(".ico-menu").click(function(){
   $("body").toggleClass("hello-class");
    
});

    


});