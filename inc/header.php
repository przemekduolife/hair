<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $title;?></title>
		<meta name="description" content="DuoLife Keratin Hair Complex" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/style_main.css" />
		<link rel="stylesheet" type="text/css" href="css/style_kontakt.css" />
		<link rel="stylesheet" type="text/css" href="css/style_sklad.css" />
		<link rel="stylesheet" type="text/css" href="css/style_keratin_hair.css" />
		<link rel="stylesheet" type="text/css" href="css/style_hair.css" />
        <link rel="stylesheet" type="text/css" href="css/draggingSlider.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<link rel="stylesheet" href="css/owl.theme.default.min.css">
		<script src="js/main.js"></script>
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(document).ready(function() {
                $(".zmianajezyka_menu").hide();

                $(".page-nav").click(function(){
                    $(".zmianajezyka_menu").hide();
                    $(".zmianajezyka_lista2").hide();
                });

                $(".zmianajezyka_linkaktywny").click(function(){
                    $(".zmianajezyka_menu").toggle();
                });

                $(".zmianajezyka_lista2").hide();

                $(".menujezykzmiana").click(function(){
                    $(".zmianajezyka_lista2").toggle();
                });
            });
        </script>
        <script>
            //scrooling-smooth
            $(document).ready(function(){
                $("a").on('click', function(event) {

                        if (this.hash !== "") {
                        event.preventDefault();
                        var hash = this.hash;

                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 1000, function(){
                            window.location.hash = hash;
                        });
                    }
                });
            });
        </script>
</head>