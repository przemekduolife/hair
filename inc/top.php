<?php
switch ($lang_menu){
    case "pl":
        $lang_main="Polski";
        $lang_src="img/jezyki_ikony/pl.png";
        break;
    case "en":
        $lang_main="English";
        $lang_src="../img/jezyki_ikony/gb.png";
        break;
}
$path = $_SERVER['PHP_SELF'];
$path_parts = pathinfo($path);
$site = $path_parts['basename'];
?>
<div class="top-tlo">
	<div class="header" id="header-smooth">
        <div class="zmianajezyka_button_container">
            <div class="zmianajezyka_linkaktywny">
                <img src="<?php echo $lang_src;?>" class="zmianajezyka_flaga_main" />
                <?php echo $lang_main;?>
                <img src=	"img/jezyki_ikony/dropdown-icon.png" />
            </div>
        </div>
        <div class="zmianajezyka_menu">
            <nav class="zmianajezyka_lista">
                <ul>
                    <li><a href="" class="zmianajezyka_link"><img src="img/jezyki_ikony/pl.png" class="zmianajezyka_flaga" />Polski</a></li>
                    <li><a href="<?php echo dirname($_SERVER['SERVER_NAME']); ?>/en/<?php echo $site;?>" class="zmianajezyka_link"><img src="img/jezyki_ikony/gb.png" class="zmianajezyka_flaga" />English</a></li>
                </ul>
            </nav>
        </div>
        <a href="/">
            <div class="logo">
                <img src="img/logo-keratin.png" title="Keratin Hair Complex" alt="Keratin Hair Complex" width ="128px" height="86px" />
            </div>
        </a>
        <nav class="page-nav">
             <div class="page-menu">
                <div class="icon-shop">
                    <a href="https://myduolife.com/shop/products/1/780,duolife-keratin-hair-complex.html" title="Nie czekaj i kup Keratin Hair Complex
                        już teraz!" class="top-sklep" target = "_blanc"><img src = "img/sklep-ikona.svg"></a>
                </div>

                <div class="hamburger" title="Open menu" alt="navigation menu">
                    <div id="nav-icon1">
                          <span></span>
                          <span></span>
                          <span></span>
                    </div>
                </div>
            </div>
                <?php
                switch ($subpage){
                    case "main":
                    $main="active";
                    break;
                    case "marka":
                    $marka="active";
                    break;
                    case "sklad":
                    $sklad="active";
                    break;
                    case "hair":
                    $hair="active";
                    break;
                    case "sklep":
                    $sklep="active";
                    break;
                    case "kontakt":
                    $kontakt="active";
                    break;
                }
                ?>
            <div class="page-menu-list">
                <ul>
                    <li class="<?php echo $main; ?>"><a href="<?php echo dirname($_SERVER['SERVER_NAME']); ?>" class="" title="Keratin Hair Complex">Strona główna</a></li>
                    <li class="<?php echo $marka; ?>"><a href="<?php echo dirname($_SERVER['SERVER_NAME']); ?>/keratin-hair" title="Keratin Hair Complex" class="">DUOLIFE KERATIN HAIR COMPLEX</a></li>
                    <li class="<?php echo $sklad; ?>"><a href="sklad" title="Keratin Hair Complex - Skład" class="">Skład</a></li>
                    <li class="<?php echo $hair; ?> menuHeader"><a href="hair" title="Keratin Hair Complex - Włosy" class="">Włosy</a>
                    </li>
                    <li class="<?php echo $kontakt; ?>"><a href="kontakt" title="Kontakt">Kontakt</a>
                    </li>
                    <li class="<?php echo $sklep; ?>"><a href="https://myduolife.com/shop/products/1/780,duolife-keratin-hair-complex.html" title="Nie czekaj i kup Keratin Hair Complex
                        już teraz!" class="menu-store" target = "_blanc" ><!--<img src = "img/sklep-ikona.svg">-->Sklep</a>
                    </li>
                </ul>
            </div>
        </nav>
	</div>
 </div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('ul.menuItem').hide();
			$(".menuHeader > a").click(function(){
				$('.menuItem:visible').slideToggle();
				$('ul', $(this).parent()).slideToggle();
			});
			
			$("li.menuHeader").hover(
			function(){
					$('ul',this).slideDown();
			},
			function(){
					$('ul',this).slideUp();
			});

		});	
    </script>



		