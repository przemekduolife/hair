<?php
$subpage="hair";
$title="Keratin Hair Complex - Hair";
$background="inherit";
@include('inc/header.php');
?>

<body>
<div id="desc1">
    <?php @include('inc/top.php'); ?>
    <div class="desc1-content">
        <div class="desc1-boxes">
            <div class="desc1-box">
                <h3><?php echo $lang['kropla_wiedzy'];?></h3>
                <div class="desc1-text">
                    <?php echo $lang['liczba_wlosow'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc2">
    <div class="desc2-content">
        <div class="desc2-boxes">
            <div class="desc8-img">
                <img  src="../img/hair-etapy-img.png" title="hair-3steps" alt="hair-3steps" />
            </div>
            <div class="desc2-box">
                <div class="desc2-box1">
                    <span><?php echo $lang['Anagen'];?></span>
                    <span><?php echo $lang['Katagen'];?></span>
                    <span><?php echo $lang['Telogen'];?></span>
                </div>
                <div class="desc2-text">
                    <?php echo $lang['Wzrost_wlosów_odbywa'];?>
                </div>
                <div class="desc2-box3">
                    <div class="desc2-box3-left">
                        <div class="step1"></div>
                        <div class="desc2-left-text phase1">
                            <?php echo $lang['Pierwsza_faza'];?>
                        </div>
                    </div>
                    <div class="desc2-box3-right">
                        <div class="step2"></div>
                        <div class="desc2-right-text phase2">
                            <?php echo $lang['Druga_faza'];?>
                        </div>
                    </div>
                    <div class="desc2-box3-left">
                        <div class="step3"></div>
                        <div class="desc2-left-text phase3">
                            <?php echo $lang['Ostatnia_faza'];?>
                        </div>
                    </div>
                </div>
                <div class="desc2-text">
                    <?php echo $lang['Rozwoj_mieszkow'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc3">
    <div class="desc3-content">
        <div class="desc3-boxes">
            <div class="desc3-box">
                <h5><?php echo $lang['Sklad_wlosa'];?></h5>
                <div class="desc3-content-img">
                    <img  src="../img/desc3-composition.png" title="hair-composition" alt="hair-composition" />
                </div>
                <div class="desc3-box-composition">
                    <div class="comp1">
                        <?php echo $lang['Keratyna'];?> 80%
                    </div>
                    <div class="comp1-height"></div>
                    <div class="comp2">
                        <?php echo $lang['Woda'];?> 10%
                    </div>
                    <div class="comp2-height"></div>
                    <div class="comp3">
                        <?php echo $lang['Tluszcze'];?> 5.8%
                    </div>
                    <div class="comp3-height"></div>
                    <div class="comp4">
                        <?php echo $lang['Cukry_aminokwasy'];?>
                    </div>
                    <div class="comp4-height"></div>

                </div>
                <div class="desc3-box-text">
                    <div>
                        <?php echo $lang['Jest_to_bialko'];?>
                    </div>
                    <div>
                        <?php echo $lang['Proces_keratynizacji'];?>
                    </div>
                    <div>
                        <?php echo $lang['Wlokna_keratynowe'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc4">
    <div class="desc4-content">
        <div class="desc4-boxes">
            <div class="desc4-text-h5">
                <h5><?php echo $lang['Wyglad_struktura'];?></h5>
            </div>
            <div class="hair3-box">
                <div class="hair3-icon-box">
                    <div>
                        <img  src="../img/hair3-img-ill.png" title="Sick hair" alt="Sick hair" />
                    </div>
                    <div class="hair3-box-text">
                        <?php echo $lang['Wlos_chory'];?><br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="../img/hair3-img-healthy.png" title="Normal hair" alt="Normal hair" />
                    </div>
                    <div class="hair3-box-text">
                        <?php echo $lang['Wlos_zdrowy'];?><br>&nbsp;
                    </div>
                </div>
                <div class="hair3-icon-box">
                    <div>
                        <img  src="../img/hair3-img-hair.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
                    </div>
                    <div class="hair3-box-text">
                        <b>DuoLife Keratin<br>Hair Complex</b>
                    </div>
                </div>

            </div>
            <div class="desc4-text">
                <h5>
                    <?php echo $lang['Sklad_jakosciowo_ilosciowy'];?>
                </h5>
                <div>
                    <?php echo $lang['Melanina_pigment'];?>
                </div>
                <div>
                    <?php echo $lang['Eumelanina_sprawia'];?>
                </div>
                <div>
                    <?php echo $lang['Feomelanina_odpowiada'];?>
                </div>
                <div>
                    <?php echo $lang['po_wakacjach'];?>
                </div>
                <div>
                    <?php echo $lang['poza_tym'];?>
                </div>
                <div>
                    <?php echo $lang['rowniez_woda'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc5">
    <div class="desc5-content">
        <div class="desc5-boxes">
            <div class="desc5-text">
                <div class="desc5-title">
                    <div class="desc5-icon-info glyphicon glyphicon-info-sign"></div>
                    <h5><?php echo $lang['Struktura_wlosa'];?></h5>
                </div>

                <div>
                    <?php echo $lang['Kod_genetyczny'];?>
                </div>
                <div>
                    <?php echo $lang['Warto_przy_tym'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc6">
    <div class="desc6-content">
        <div class="desc6-boxes">
            <h5>
                <?php echo $lang['Przekrój_poprzeczny'];?>
            </h5>
            <div class="desc6-box">
                <div class="desc6-box-comp">
                    <p class="desc6-comp1"><?php echo $lang['warstwa_rdzenna'];?></p>
                    <div class="desc6-index1"><span class="index-number">1</span></div>
                    <div class="desc6-line1"></div>
                    <div class="desc6-index1-square"><span class="index-number">1</span></div>
                    <p class="desc6-comp2"><?php echo $lang['warstwa_korowa'];?></p>
                    <div class="desc6-index2"><span class="index-number">2</span></div>
                    <div class="desc6-line2"></div>
                    <div class="desc6-index2-square"><span class="index-number">2</span></div>
                    <p class="desc6-comp3"><?php echo $lang['powloczka_wlosa'];?></p>
                    <div class="desc6-index3"><span class="index-number">3</span></div>
                    <div class="desc6-line3"></div>
                    <div class="desc6-index3-square"><span class="index-number">3</span></div>
                    <p class="desc6-comp4"><?php echo $lang['oslonka_pochewki'];?></p>
                    <div class="desc6-index4"><span class="index-number">4</span></div>
                    <div class="desc6-line4"></div>
                    <div class="desc6-index4-square"><span class="index-number">4</span></div>
                    <p class="desc6-comp5"><?php echo $lang['warstwa_ziarnista'];?></p>
                    <div class="desc6-index5"><span class="index-number">5</span></div>
                    <div class="desc6-line5"></div>
                    <div class="desc6-index5-square"><span class="index-number">5</span></div>
                    <p class="desc6-comp6"><?php echo $lang['warstwa_jasna'];?></p>
                    <div class="desc6-index6"><span class="index-number">6</span></div>
                    <div class="desc6-line6"></div>
                    <div class="desc6-index6-square"><span class="index-number">6</span></div>
                    <p class="desc6-comp7"><?php echo $lang['pochewka_zewnetrzna'];?></p>
                    <div class="desc6-index7"><span class="index-number">7</span></div>
                    <div class="desc6-line7"></div>
                    <div class="desc6-index7-square"><span class="index-number">7</span></div>
                    <p class="desc6-comp8"><?php echo $lang['brodawka_wlosa'];?></p>
                    <div class="desc6-index8"><span class="index-number">8</span></div>
                    <div class="desc6-line8"></div>
                    <div class="desc6-index8-square"><span class="index-number">8</span></div>
                    <p class="desc6-comp9">c<?php echo $lang['cebulka_wlosa'];?></p>
                    <div class="desc6-index9"><span class="index-number">9</span></div>
                    <div class="desc6-line9"></div>
                    <div class="desc6-index9-square"><span class="index-number">9</span></div>
                </div>
            </div>
            <div class="desc6-text">
                <div>
                    <?php echo $lang['Kod_decyduje'];?>
                </div>
                <div>
                    <?php echo $lang['Warto_podkreslic'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc7">
    <div class="desc7-content">
        <div class="desc7-boxes">
            <div class="desc7-box">
                <h5>
                    <?php echo $lang['jaki_jest_skręt'];?>
                </h5>
                <div class="desc7-text">
                    <?php echo $lang['odpowiednia_pielegnacja'];?>
                </div>
                <div class="desc7-exp">
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">1</span></div>
                            <img  src="../img/experiment-001.png" title="experiment 1" alt="experiment 1" />
                        </div>
                        <div class="desc7-exp-text">
                            <?php echo $lang['Narysuj'];?>
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">2</span></div>
                            <img  src="../img/experiment-002.png" title="experiment 2" alt="experiment 2" />
                        </div>
                        <div class="desc7-exp-text">
                            <?php echo $lang['Wyrwij'];?>
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">3</span></div>
                            <img  src="../img/experiment-003.png" title="experiment 3" alt="experiment 3" />
                        </div>
                        <div class="desc7-exp-text">
                            <?php echo $lang['Zamocz'];?>
                        </div>
                    </div>
                    <div class="desc7-exp-box">
                        <div class="desc7-img">
                            <div class="desc7-index1-square"><span class="desc7-index-number">4</span></div>
                            <img  src="../img/experiment-004.png" title="experiment 4" alt="experiment 4" />
                        </div>
                        <div class="desc7-exp-text">
                            <?php echo $lang['Uloz'];?>
                        </div>
                    </div>
                </div>
                <h5>
                    <?php echo $lang['Wyniki'];?>
                </h5>
                <div class="desc7-text">
                    <div>
                        1. <?php echo $lang['mozesz_zaliczyc'];?>
                    </div>
                    <div>
                        2. <?php echo $lang['obrys_kolka'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="desc8">
    <div class="desc8-content">
        <div class="desc8-boxes">
            <div class="desc8-img">
                <img src="../img/desc8-image.png" title = "A common problem of hair loss" alt = "A common problem of hair loss">
            </div>
            <div class="desc8-box">
                <h5>
                    <?php echo $lang['Powszechny_problem'];?>
                </h5>
                <div class="desc8-text">
                    <?php echo $lang['Opisane_uprzednio'];?>
                </div>
                <div class="desc8-list">
                    <div class="desc8-list-left">
                        <ul>
                            <li><?php echo $lang['permanentny_stres'];?></li>
                            <li><?php echo $lang['nieprawidlowa_dieta'];?></li>
                            <li><?php echo $lang['zaburzenia_hormonalne'];?></li>
                        </ul>
                    </div>
                    <div class="desc8-list-right">
                        <ul>
                            <li><?php echo $lang['niewlasciwa_pielegnacja'];?></li>
                            <li><?php echo $lang['genetyka'];?></li>
                        </ul>
                    </div>
                </div>
                <h5>
                    <?php echo $lang['zaburzonym_cyklu'];?>
                </h5>
                <div class="desc8-text">
                    <div>
                        1. <?php echo $lang['proporcje_wlosow_anagenowych'];?>
                    </div>
                    <div>
                        2. <?php echo $lang['wlosow_anagenowych'];?>
                    </div>
                    <div>
                        3. <?php echo $lang['Skraca_czas'];?>
                    </div>
                    <div>
                        4. <?php echo $lang['zastepcze_wlosy'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php @include('inc/footer.php'); ?>
</body>
</html>