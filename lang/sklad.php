<?php
$subpage="sklad";
$title="Keratin Hair Complex-Skład";
$background="inherit";
@include('inc/header.php');
?>

<body>
<div id="composition1">
    <?php @include('inc/top.php');?>
    <div class="composition1-content">

        <h3><?php echo $lang['Wlos_suchy'];?>Kompleksowe działanie w&nbsp;3&nbsp;krokach</h3>

        <div class="composition1-box">
            <div class="composition1-icon-box">
                <a href ="#component2-content">
                    <div class="composition1-button-step">
                        <?php echo $lang['krok1'];?>
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    <?php echo $lang['pomaga_wlosom_rosnac'];?>
                </div>
            </div>
            <div class="composition1-icon-box">
                <a href ="#component3-content">
                    <div class="composition1-button-step">
                        <?php echo $lang['krok2'];?>
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    <?php echo $lang['hamuje_wypadanie_wlosow'];?>
                </div>
            </div>
            <div class="composition1-icon-box">
                <a href ="#component4-content">
                    <div class="composition1-button-step">
                        <?php echo $lang['krok3'];?>
                    </div>
                </a>
                <div class="composition1-box-text">
                    <b>Keratin Hair Complex</b>
                    <?php echo $lang['dodaje_wlosom_gestosci'];?>
                </div>
            </div>
        </div>
    </div>
    <div id="component2-content">

        <div class="component2-menu">
            <div class="component2-step1">
                <?php echo $lang['krok1'];?>
            </div>
            <div class="component2-break">
            </div>
            <a href="#component3-content">
                <div class="component2-step2">
                    <?php echo $lang['krok2'];?>
                </div>
            </a>
            <div class="component2-break"></div>
            <a href="#component4-content">
                <div class="component2-step3">
                    <?php echo $lang['krok3'];?>
                </div>
            </a>
        </div>
        <div class="component2-boxes">
            <div class="component2-img">
                <img  src="../img/component-step1.png" title="Keratin Hair Complex helps hair grow" alt="Keratin Hair Complex helps hair grow" />
            </div>
        </div>
        <div class="component2-boxes">
            <div class="component2-text">
                <h5>
                    <b>Keratin Hair Complex</b> <?php echo $lang['pomaga_wlosom_rosnac'];?>
                </h5>
                <div>
                    <?php echo $lang['Ekstrakt_z_czarnego_ryzu'];?>
                </div>
                <ul>
                    <li>
                        <?php echo $lang['wzmocnienie_zdolnosci_komorek'];?>
                    </li>
                    <li>
                        <?php echo $lang['pobudzanie_wlosow'];?>
                    </li>
                    <li>
                        <?php echo $lang['ochrone_wlosow'];?>
                    </li>
                </ul>
                <div>
                    <?php echo $lang['W_skladzie_produktu'];?>
                </div>
                <ul>
                    <li>
                        <?php echo $lang['Bialko_Noggin'];?>
                    </li>
                    <li>
                        <?php echo $lang['czynnik_wzrostu'];?>
                    </li>
                </ul>
            </div>
        </div>
        <div class="component2-boxes2">
            <div class="component2-text2">
                <div class="component2-phrase-first">
                    <div class="component2-icon-info glyphicon glyphicon-info-sign"></div>
                    <?php echo $lang['Analiza_ekspresji'];?>
                </div>
                <ul>
                    <li>
                        <?php echo $lang['znacznego_zmniejszenia_utraty'];?>
                    </li>
                    <li>
                        <?php echo $lang['utraty_wlosow'];?>
                    </li>
                    <li>
                        <?php echo $lang['zwiekszonej_gestosci_wlosow'];?>
                    </li>
                    <li>
                        <?php echo $lang['redukcji_wypadania'];?>
                    </li>
                    <li>
                        <?php echo $lang['ochotnikow_wykazalo'];?>
                    </li>
                </ul>
                <div>
                    <?php echo $lang['Grupa_ochotnikow'];?>
                </div>
                <ul>
                    <li>
                        <?php echo $lang['Naturalny_produkt'];?>
                    </li>
                    <li>
                        <?php echo $lang['Produkt_opracowany'];?>
                    </li>
                    <li>
                        <?php echo $lang['Kliniczna_skutecznosc'];?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="component3-content">
        <div class="component3-menu">
            <a href="#component2-content">
                <div class="component3-step1">
                    <?php echo $lang['krok1'];?>
                </div>
            </a>
            <div class="component3-break1">
            </div>
            <div class="component3-step2">
                <?php echo $lang['krok2'];?>
            </div>
            <div class="component3-break2"></div>
            <a href="#component4-content">
                <div class="component3-step3">
                    <?php echo $lang['krok3'];?>
                </div>
            </a>
        </div>
        <div class="component3-boxes">
            <div class="component3-img">
                <img  src="../img/component-step2.png" title="Keratin Hair Complex inhibits hair loss" alt="Keratin Hair Complex inhibits hair loss" />
            </div>
        </div>
        <div class="component3-boxes">
            <div class="component3-text">
                <h5>
                    <?php echo $lang['hamuje_wypadanie'];?>
                </h5>
                <div>
                    <?php echo $lang['kompleksowa_odpowiedz'];?>
                </div>
                <div class="component3-przypis">
                    * <?php echo $lang['dotyczy_wybranych_składnikow'];?>
                </div>
            </div>
        </div>
    </div>
    <div id="component4-content">
        <div class="component4-menu">
            <a href="#component2-content">
                <div class="component4-step1">
                    <?php echo $lang['krok1'];?>
                </div>
            </a>
            <div class="component4-break1">
            </div>
            <a href="#component3-content">
                <div class="component4-step2">
                    <?php echo $lang['krok2'];?>
                </div>
            </a>
            <div class="component4-break2"></div>
            <div class="component4-step3">
                <?php echo $lang['krok3'];?>
            </div>
        </div>
        <div class="component4-boxes">
            <div class="component4-img">
                <img  src="../img/component-step3.png" title="Keratin Hair Complex adds density and volume to the hair" alt="Keratin Hair Complex adds density and volume to the hair" />
            </div>
        </div>
        <div class="component4-boxes">
            <div class="component4-text">
                <h5>
                    <?php echo $lang['dodaje_wlosom'];?>
                </h5>
                <div>
                    <?php echo $lang['Wlosy_potrzebuja_odpowiedniej'];?>
                </div>
                <div>
                    <?php echo $lang['występuja_owoce_amli'];?>
                </div>
                <div>
                    <?php echo $lang['Witamina_C_funkcja'];?>
                </div>
                <div>
                    <?php echo $lang['Zawarty_w_produkcie_ekstrakt'];?>
                </div>
            </div>
        </div>
    </div>
    <div class="composition3">
        <div class="composition3-content">

            <h3>
                <?php echo $lang['Zobacz_jak_dziala'];?><br>DuoLife Keratin Hair Complex
            </h3>
            <div><a href="https://youtu.be/8a8DR1JEs9w" target="_blank">
                    <img  src="../img/play-now-icon.svg" title="see" alt="see" />
                </a>
            </div>
            <div class="composition-bottle-left">
                <img  src="../img/composition-bottle-left.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
            </div>
            <div class="composition-bottle-right">
                <img  src="../img/composition-bottle-right.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
            </div>

        </div>
    </div>
    <div class="composition4">
        <div class="composition4-content">
            <div class="composition4-kropla">
                <!--<img src="../img/kropla.svg">-->
            </div>
            <div class="composition4-left-text">
                <h3><?php echo $lang['Zweryfikuj_efekty'];?>
                </h3>
                <div>
                    <?php echo $lang['Przesun_linie'];?>
                </div>
            </div>
            <div class="wrapper">
                <div class="before">
                    <img class="content-image" src="../img/hair-compare-new4.png" draggable="false"/>
                    <div class="after">
                        <img class="content-image" src="../img/hair-compare-old4.png" draggable="false"/>
                    </div>
                </div>
                <div class="scroller">
                    <svg class="scroller__thumb" xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100"><polygon points="0 50 37 68 37 32 0 50" style="fill:#fff"/><polygon points="100 50 64 32 64 68 100 50" style="fill:#fff"/></svg>
                </div>
            </div>
        </div>
    </div>
    <div class="composition5">
        <div class="composition5-content">
            <div class="composition5-boxes">
                <div class="composition5-img">
                    <img  src="../img/composition5-img.png" title="Comprehensively, i.e. not only Hair Therapy" alt="Comprehensively, i.e. not only Hair Therapy" />
                </div>
            </div>
            <div class="composition5-boxes">
                <div class="composition5-text">
                    <h5>
                        <?php echo $lang['Kompleksowo_Terapia_Wlosa'];?>
                    </h5>
                    <div>
                        <?php echo $lang['Warto_podkreslic'];?>
                    </div>
                    <div>
                        <?php echo $lang['Dzieki_wyciagowi'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="composition6">
        <div class="composition6-content">
            <div class="composition6-boxes">
                <div class="composition6-text">
                    <div class="composition6-text-skladniki">
                        <b><?php echo $lang['Skladniki'];?>: </b> <?php echo $lang['Skladniki_lista'];?>
                    </div>
                    <div class="composition6-table">
                        <table cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <th><?php echo $lang['Skladniki'];?></th>
                                <th>25 ml</th>
                                <th>50 ml</th>
                                <th>100 ml</th>
                            </tr>
                            <tr>
                                <td><?php echo $lang['Witamina_C'];?></td>
                                <td>50 mg</td>
                                <td>100 mg</td>
                                <td>200 mg</td>
                            </tr>
                            <tr>
                                <td><?php echo $lang['RWS'];?>*</td>
                                <td>63%</td>
                                <td>125%</td>
                                <td>250%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="composition6-przypis">
                        *<?php echo $lang['RWS'];?> - <?php echo $lang['Referencyjna_wartosc'];?>
                    </div>
                    <div>
                        <b><?php echo $lang['Objetosc'];?></b> 750ml
                    </div>
                    <div>
                        <?php echo $lang['Dawkownie'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php @include('inc/footer.php'); ?>
</body>
</html>