<?php
//top, footer
$lang['strona_glowna']="Strona główna";
$lang['sklad']="Skład";
$lang['wlosy']="Włosy";
$lang['kontakt']="Kontakt";
$lang['sklep']="Sklep";
$lang['wyprodukowano']="Wyprodukowano w UE dla:";
$lang['pobierz_ulotke']="pobierz ulotkę";

//main
$lang['to_kontynuacja_produktow']="<b>DuoLife Keratin Hair Complex</b> to kontynuacja produktów płynnych. To suplement diety w formie płynnej na bazie keratyny, zawierający naturalnie składniki pochodzenia roślinnego. Są one opatrzone  międzynarodowymi patentami*. W 100% naturalne, przebadane klinicznie*. Ich skuteczność została potwierdzona badaniami dotyczącymi wzrostu i kondycji włosa. Surowce użyte w procesie formulacji mają udowodnioną skuteczność w przypadku zastosowania doustnego i przeznaczony jest do stosowania przez kobiety i mężczyzn*.";
$lang['dotyczy_wybranych_składnikow']="dotyczy wybranych składników";
$lang['zdrowie_i_piekno']="DuoLife Keratin Hair Complex zdrowie i piękno w jednym";
$lang['Wypadanie_Twoich_wlosow']="Wypadanie Twoich włosów zmniejszone o 34% już po miesiącu stosowania*";
$lang['Znaczna_redukcja_wypadania']="Znaczna redukcja wypadania włosów o 86% po pełnej kuracji*";
$lang['Twoje_wlosy_sa']="Twoje włosy są 10x mocniejsze i 2x bardziej sprężyste, niż przed terapią*";
$lang['Znaczna_poprawa_objetosci']="Znaczna poprawa objętości i gęstości XXL dla włosów cienkich*";
$lang['Rezultaty_dzialania_widoczne']="Rezultaty działania widoczne po 3 miesiącach stosowania*";
$lang['Badania_przeprowadzone_na']="Badania przeprowadzone na grupie 21 osób. Weryfikacja działania sprawdzana co 2 tygodnie stosowania.<br>Badanie wykonane w labolatorium badawczym.";
$lang['Poznaj_zalety_Keratin']="Poznaj zalety DuoLife Keratin Hair Complex";
$lang['zawiera_plynna_kreatynę']="zawiera solubilizowaną, płynną kreatynę";
$lang['kliniczna_skutecznosc_icon']="kliniczna skuteczność*";
$lang['surowce_posiadaja_patenty']="surowce posiadają międzynarodowe patenty*";
$lang['naturalny']="100% naturalny";
$lang['nie_zawiera_substancji']="nie zawiera substancji konserwujących";
$lang['nie_zawiera_produktow']=" nie zawiera produktów genetycznie modyfikowanych";
$lang['zawiera_naturalnych_skladnikow']="DuoLife Keratin Hair Complex zawiera<br>100%&nbsp;naturalnych składników";
$lang['owoce_gujawy']="puree z owoców gujawy";
$lang['owoce_amli']="ekstrakt z owoców amli";
$lang['keratyna_solubilizowana']="keratyna solubilizowana";
$lang['pestki_winogron']="ekstrakt z pestek winogron";
$lang['czarny_ryz']="formuła ekstraktu z czarnego ryżu i kwiatów opuncji figowej ";
$lang['groch']="ekstrakt z organicznych pędów grochu ";
$lang['pokrzywa']="ekstrakt z liści pokrzywy zwyczajnej ";
$lang['skrzyp_polny']="ekstrakt z ziela skrzypu polnego ";
$lang['maliny']="Sok z owoców z malin";
$lang['plynna_keratyna']="100% solubilizowana,<br>płynna keratyna";
$lang['Kup_teraz']="Kup teraz";
$lang['pobierz_ulotke']="pobierz ulotkę";

//duolife keratin hair complex

$lang['Dla_kogo_zostal']="Dla kogo został stworzony Keratin Hair Complex";
$lang['Wypadanie_wlosów_to']="Wypadanie włosów to proces fizjologiczny, wynikający z naturalnego cyklu rozwojowego włosa. Niestety, nadmierne wypadanie sugeruje niekorzystne zmiany zachodzące w obrębie nie tylko włosów, ale i organizmu. W naszym ciele bowiem dochodzi do zaburzeń spowodowanych stresem, brakiem równowagi hormonalnej, działaniem czynników środowiskowych, ale i wiekiem oraz dziedziczeniem. Gdy skóra głowy staje się  słabiej ukrwiona, a kosmyki niedostatecznie odżywione, dochodzi do ich osłabienia, a co za tym idzie, okresowego lub przewlekłego wypadania. Włosy pozbawoine witalności tracą wytrzymałość i gęstość";
$lang['W_przypadku_procesu']="W przypadku procesu fizjologicznego, włosy wypadają z częstotliwością około 60-100 kosmyków na dobę. Na ich miejsce wzrastają nowe. Może zdarzyć się tak, że nawet te nowe będą kruche, łamliwe, pozbawione blasku i bez wyraźnej gęstości";
$lang['stworzylismy_po_to']="Keratin Hair Complex</b> stworzyliśmy po to, by o swoje włosy mogly zadbać osoby zmagające się z cienkimi i delikatnymi włosami. To sosób na odżywienie i wzmocnienie, dzięki formule kompleksu keratynowego. Łączy on unikatowe wartości naturalnych wyciągów roślinnych.";
$lang['wplywaja_na_wzmocnienie']="Składniki <b>Keratin Hair Complex</b> wpływają na wzmocnienie u nasady, przyspieszenie wzrostu i zapobieganie przedwczesnemu wypadaniu. Naturalne ekstrakty roślinne powodują wygładzenie włosów sprawiając, że stają się one sprężyste i lśniące. Kliniczna formuła Keratin Hair Complex i najnowsze technologie pozwoliły na stworzenie produktu, którego skład decyduje o nawilżeniu i&nbsp;odżywieniu włosów, czyniąc je mniej łamliwymi i ograniczając wypadanie.";
$lang['Wlos_suchy']="Włos suchy";
$lang['Wlos_zdrowy']="Włos zdrowy";



$lang['Kompleksowe_dzialanie_kroki']="Kompleksowe działanie w&nbsp;3&nbsp;krokach";
$lang['krok1']="Krok 1";
$lang['krok2']="Krok 2";
$lang['krok3']="Krok 3";
$lang['pomaga_wlosom_rosnac']="pomaga włosom rosnąć";
$lang['hamuje_wypadanie_wlosow']="hamuje wypadanie włosów";
$lang['dodaje_wlosom_gestosci']="dodaje włosom gęstości i&nbsp;objętości";
$lang['pomaga_wlosom_rosnac']="pomaga włosom rosnąć";
$lang['Ekstrakt_z_czarnego_ryzu']="Ekstrakt z czarnego ryżu i opuncji figowej wpływają na:";
//sklad
$lang['wzmocnienie_zdolnosci_komorek']=" wzmocnienie zdolności komórek do walki ze stresem oksydacyjnym i ochronę przed skutkami zaburzeń hormonalnych,";
$lang['pobudzanie_wlosow']="pobudzanie włosów do wzrostu poprzez indukcję do namnażania komórek brodawkowatych występujących w mieszkach włosowych. Efektem są tzw.\"baby hair\". Czyli widoczne, młode włoski, które zaczynają rosnąć. Najczęściej porastają wyższe partię czoła, choć mogą uwidocznić się również w okolicy szyi. Przyczyniają się do unoszenia włosów u nasady. Nowopowstałe włoski są sprężyste, wytrzymałe i wyglądają zdrowo.";
$lang['ochrone_wlosow']="ochronę włosów przed biologicznymi mechanizmami odpowiedzialnymi za ich wypadanie, dzięki właściwościom czarnego ryżu i mieszkanki ekstraktu z kwiatów opuncji poprzez połączenie efektu antyoksydacyjnego z hamowaniem 5-α-reduktazy,";
$lang['W_skladzie_produktu']="W składzie produktu znajduje się również ekstrakt z organicznych pędów grochu opracowany zgodnie z zasadami standaryzacji biologicznej w oparciu o technologię mikromacierzy DNA. Wpływa on na stymulację cząsteczek sygnalizujących w komórkach brodawek skórnych, decydujących o wzroście nowego włosa:";
$lang['Bialko_Noggin']="Białko Noggin, białko skracające fazę telogenu";
$lang['Analiza_ekspresji']="Analiza ekspresji genów została przeprowadzona na wyrwanych cebulkach włosów po dwutygodniowej kuracji z zastosowaniem surowca. Wyniki badania wykazały 56% wyższą regulację pozytywną Noggin i 85% wyższą regulację FGF-7. Wyniki te pokazują, że ekstrakt z organicznych pędów grochu zastosowany w Keratin Hair Complex jest w stanie stymulować brodawki skórne do wywoływania wzrostu nowych włosów.";
$lang['znacznego_zmniejszenia_utraty']="znacznego zmniejszenia utraty włosów po miesiącu (-34%),";
$lang['utraty_wlosow']="znacznego zmniejszenia utraty włosów po dwóch miesiącach";
$lang['zwiekszonej_gestosci_wlosow']="zwiększonej gęstości włosów,";
$lang['redukcji_wypadania']="znacznej redukcji wypadania włosów (86%),";
$lang['ochotnikow_wykazalo']="76% ochotników wykazało chęć kontynuowania stosowania surowca.";
$lang['Grupa_ochotnikow']="Grupa ochotników zażywała dzienną dawkę 100 mg produktu <b>– taką samą, jaką zastosowano w formulacji produktu DuoLife.</b>";
$lang['Naturalny_produkt']="Naturalny produkt wspomagający wzrost włosów przygotowany z jadalnych, organicznych kiełków grochu (Pisum sativum)";
$lang['Produkt_opracowany']="Produkt opracowany zgodnie z zasadami standaryzacji biologicznej: lepsza ekspresja FGF7 i Nogginy równoważąca cykl wzrostu włosa";
$lang['Kliniczna_skutecznosc']="Kliniczna skuteczność udowodniona w przypadku zastosowania doustnego i aplikacji miejscowej u osób obu płci";
$lang['hamuje_wypadanie']="Keratin Hair Complex hamuje wypadanie włosów";
$lang['kompleksowa_odpowiedz']="Składniki użyte w Keratin Hair Complex to kompleksowa odpowiedź na problem wypadających, słabych, cienkich i przerzedzających się włosów. Unikalna kompozycja składnikowa została dobrana tak, aby zoptymalizować proces stymulacji i odbudowy włosa. Wszystkie użyte składniki uzupełniają się wzajemnie, tworząc płynną Keratynową Terapię Odżywczą Włosa. Suplement diety zapobiega nadmiernemu wypadaniu włosów. Użyte składniki, takie jak ekstrakt z czarnego ryżu i opuncji figowej, ekstrakt z organicznych pędów grochu czy keratyna solubilizowana, zostały dobrane tak, aby kompleksowo wspomagać walkę z wypadaniem włosów. Ich wielokierunkowe działanie ponadto wzmacnia i poprawia ich kondycję. Zawarte w produkcie składniki posiadają kliniczną skuteczność*.";
$lang['dodaje_wlosom']="Keratin Hair Complex dodaje włosom gęstości i objętości";
$lang['Wlosy_potrzebuja_odpowiedniej']="Włosy potrzebują odpowiedniej ilości przyswajalnej siarki, która zawarta jest w krzemionce. Bogatym jej źródłem są skrzyp polny oraz pokrzywa. Systematyczne stosowanie produktów z ich zawartością pozwala widocznie poprawić stan włosów. Można liczyć na ograniczenie wypadania, wzmocnienie, odżywienie i regenerację.";
$lang['występuja_owoce_amli']="W produkcie <b>Keratin Hair Complex</b> występują owoce amli, które są źródłem ważnej dla włosów witaminy C. Jej rolą jest zapewnienie szczelności naczyniom krwionośnym, stymulacja przepływu krwi w organizmie, co prowadzi do efektywnego transportu substancji odżywczych do włosów. Niedobór witaminy C przyczynia się do pogorszenia funkcjonowania skóry głowy, co sprawia, że proces tworzenia łodygi włosa zostaje zaburzony i pojawia się problem z wypadaniem włosów. Ten stan jest także podyktowany niedoborami żelaza w organizmie, a jego poziom systematycznie spada, gdy w organizmie występuje zbyt mało witaminy C.";
$lang['Witamina_C_funkcja']="Witamina C to kolejna funkcja, jaką jest produkcja kolagenu – podstawowego budulca włosów odpowiedzialnego za ich elastyczność i sprężystość.";
$lang['Zawarty_w_produkcie_ekstrakt']="Zawarty w produkcie ekstrakt z pestek winogron, które stanowią bogate żródło aktywnych biologicznie oligomerycznych proantocyjanidyn (OPC - oligomericproanthocyanidincomplexes), wpływa na odżywianie i stymulację cebulek włosów. Składniki te decydują o ulepszonej strukturze włosa i uszczelnieniu jego warstwy ochronnej.";
$lang['Zobacz_jak_dziala']="Zobacz jak działa";
$lang['Zweryfikuj_efekty']="Zweryfikuj efekty<br>Twojej 3-miesięcznej<br>kuracji";
$lang['Przesun_linie']="Przesuń linię, aby zobaczyć jak DuoLife Keratin Hair Complex wpływa na wygląd włosów po 3-miesięcznej kuracji.";
$lang['Kompleksowo_Terapia_Wlosa']="Kompleksowo, czyli nie tylko Terapia Włosa";
$lang['Warto_podkreslic']="Warto podkreślić, że produkt działa dobroczynnie nie tylko na włosy, lecz również na skórę i paznokcie.";
$lang['Dzieki_wyciagowi']="Dzięki wyciągowi z czarnego ryżu i opuncji figowej Keratin Hair Complex wykazuje synergistyczne działanie przeciwko trądzikowi poprzez wzmocnienie zdolności komórek do walki ze stresem oksydacyjnym oraz ochronę przed skutkami zaburzeń hormonalnych. Przeciwdziała efektom wywołanym działaniem testosteronu w sebocytach skóry twarzy. Połączenie efektu antyoksydacyjnego z hamowaniem 5-α-reduktazy powoduje synergistyczne działanie ochronne czarnego ryżu i mieszanki ekstraktu z kwiatów opuncji przeciwko mechanizmom biologicznym odpowiedzialnym za powstawanie trądziku. Solubilizowana keratyna to rewolucyjny składnik nutrikosmeceutyczny. Surowiec uzupełnia organizm o biodostępną keratynę, która pomaga w ochronie i naprawie uszkodzonych tkanek paznokci oraz skóry.";
$lang['Skladniki']="Składniki";
$lang['Skladniki_lista']="puree z owoców gujawy, sok z owoców malin, formuła ekstraktu z czarnego ryżu i kwiatów opuncji figowej standaryzowana na zawartość antocyjanów (250mg/50ml), ekstrakt z owoców amli standaryzowany na zawartość witaminy C, ekstrakt z organicznych pędów grochu (100mg/50ml), keratyna solubilizowana (100mg/50ml), ekstrakt z liści pokrzywy zwyczajnej (70mg/50ml), ekstrakt z pestek winogron standaryzowany na zawartość polifenoli (50mg/50ml), ekstrakt z ziela skrzypu polnego (50mg/50ml).";
$lang['Witamina_C']="Witamina C";
$lang['RWS']="RWS";
$lang['Referencyjna_wartosc']="Referencyjna wartość spożycia dla przeciętnej osoby dorosłej (8400kJ/2000kcal)";
$lang['Objetosc']="Objętość:";
$lang['Dawkownie']="Dawkownie: 25ml do 50ml raz dziennie przed posiłkiem";

//hair
$lang['kropla_wiedzy']="Wzrost włosów – <br>kropla wiedzy o...";
$lang['liczba_wlosow']="<b>100-150 tysięcy</b> - to liczba włosów na głowie człowieka.  Czy wiesz, że na 1 cm2 występuje od 150 do 500 włosów ciemnych i od 180 do 750 włosów jasnych? Wystarczy 1 dzień, by na głowie włosy urosły o 0,35 mm. W ciągu miesiąca przyrost wynosi około 1 cm, co daje nawet 12 cm rocznie. Mówimy około, bowiem na wzrost włosów mają wpływ czynniki zewnętrzne. W tym samym okresie – jednego roku, z głowy człowieka może wypaść nawet 35 tysięcy włosów.";
$lang['Anagen']="Anagen";
$lang['Katagen']="Katagen";
$lang['Telogen']="Telogen";
$lang['Wzrost_wlosów_odbywa']="Wzrost włosów odbywa się w trzech fazach. Cały cykl charakteryzuje się czasem asynchronicznym, co oznacza, że nowe włosy pojawiają się na głowie w regularnych odstępach czasu i nie dochodzi do okresowego łysienia.";
$lang['Pierwsza_faza']="Pierwsza faza wzrostu włosów to anagen. Etap najważniejszy, ponieważ jest to faza aktywna. W niej zaś znajduje się aż 85-90% włosów. W anagenie włos trwale łączy się z mieszkiem włosowym. Zostaje odżywiony i przyrasta na długość. W zależności od uwarunkowań genetycznych i trybu życia, u zdrowej osoby wzrost włosów na głowie trwa średnio 6 lat.";
$lang['Druga_faza']="Druga faza to katagen, charakteryzujący się obumieraniem włosów, co polega na odłączeniu się mieszka włosowego. Stopniowe obkurczanie cebulki włosowej prowadzi do wysuwania włosa z mieszka włosowego. Ta faza średnio trwa 3 tygodnie, ale znajduje się w niej zaledwie 1-3% włosów.";
$lang['Ostatnia_faza']="Ostatnia faza – spoczynku mieszka włosowego – to telogen, w którym znajduje się około 8-15% włosów. W tym czasie włosy nie rosną, a mieszek włosowy pozostaje pusty. Telogen trwa nawet kilka miesięcy.";
$lang['Rozwoj_mieszkow']="Rozwój mieszków włosowych ma swój początek w życiu płodowym. Około 60 dnia życia zaczynają się tworzyć mieszki włosowe, ale cały cykl wzrostu włosa, zależny jest od wielu czynników. To dlatego powszechnie uważa się, że stan włosów odzwierciedla kondycję organizmu.";
$lang['Sklad_wlosa']="Skład włosa";
$lang['Keratyna']="Keratyna";
$lang['Woda']="Woda";
$lang['Tluszcze']="Tłuszcze";
$lang['Cukry_aminokwasy']="Cukry, aminokwasy i sole mineralne 4.2%";
$lang['Jest_to_bialko']="Około 80% każdego włosa stanowią włókna keratynowe będące podstawą ich składu. Jest to białko zbudowane z aminokwasów połączonych włóknami peptydowymi. Keratyna we włosie może mieć postać eukeratyny, która charakteryzuje się elastycznością, sprężystością i dużą rozciągliwością i jest najbardziej pożądana spośród wszystkich. Druga postać do pseudokeratyna występująca w wyciągniętych włóknach keratyny.";
$lang['Proces_keratynizacji']="Proces keratynizacji polega na wymianie obumarłych komórek na żywe, w których zawiera się keratyna. Dzięki niemu skóra, ale i włosy oraz paznokcie przez cały czas podlegają zamianie z obumarłych na żywe, zawierające keratynę. Powstające kertynocyty spełniają funkcje ochronne na skórę, włosy i paznokcie. Skutkiem tych działań jest młody, świeży i zadbany wygląd.";
$lang['Wlokna_keratynowe']="Włókna keratynowe tworzące keratynę są powiązane ze sobą wiązaniami siarczkowymi, co wpływa na elastyczność i sprężystość włosów oraz skóry. Keratyna obecna w strukturze włosa nie rozpuszcza się w wodze, ale niestety podatna jest na działanie wysokiej temperatury. Łatwo ją zniszczyć, np. przegrzewając włosy prostownicą, suszarką, lokówką czy też stosując niewłaściwe preparaty.";
$lang['Wlos_chory']="Włos chory";
$lang['Wyglad_struktura']="Wygląd i struktura włosów";
$lang['Sklad_jakosciowo_ilosciowy']="Skład jakościowo-ilościowy produktu DuoLife Keartin Hair Complex to odwzorowanie składu włosa";
$lang['Melanina_pigment']="Melanina, pigment wytwarzany przez melanocyty i znajdujący się w macierzy włosa, decyduje o ich kolorze. Im większa jej ilość, tym ciemniejsze są włosy. Melanina występuje w dwóch postaciach.";
$lang['Eumelanina_sprawia']="Eumelanina sprawia, że nasze włosy są ciemne lub jasne – czyli odpowiada za barwę włosów.";
$lang['Feomelanina_odpowiada']="Feomelanina odpowiada za odcień włosów – czyli czy jest za ciepły czy chłodny.";
$lang['po_wakacjach']="Jak to się dzieje, że po wakacjach w słonecznych klimatach nasze włosy jaśnieją? Sprawcą jest ponownie melanina, która jako białko wykazuje wrażliwość na działanie czynników środowiskowych oraz środków chemicznych.";
$lang['poza_tym']="A co poza tym?";
$lang['rowniez_woda']="W składzie włosów znajduje się również woda, której zawartość wynosi około 10%. Jej obecność jest bardzo istotna, gdyż tylko dobrze nawilżone włosy wyglądają zdrowo. Ponadto pozostałe składniki to: tłuszcze stanowiące 5,8%. Do tego cukry, aminokwasy i sole mineralne.";
$lang['Struktura_wlosa']="Struktura włosa warunkowana przez geny";
$lang['Kod_genetyczny']="Kod genetyczny decyduje nie tylko o naszym zdrowiu, ale i wyglądzie włosów. Cecha w postaci prostych lub kręconych włosów, powstaje ze względu na obecność genu nazwanego trichohyalinem. Jeśli jego równowaga aminokwasowa ulega zaburzeniu, włosy zaczynają się kręcić.";
$lang['Warto_przy_tym']="Warto przy tym podkreślić, że rodzaj włosów to cecha, która może zostać przekazana nie tylko w linii prostej. Oznacza to, że dziecko może mieć kręcone włosy nawet wtedy, gdy oboje rodziców mają włosy proste.";
$lang['Przekrój_poprzeczny']="Przekrój poprzeczny włosa ma znaczenie";
$lang['warstwa_rdzenna']="warstwa rdzenna";
$lang['warstwa_korowa']="warstwa korowa";
$lang['powloczka_wlosa']="powłoczka włosa";
$lang['oslonka_pochewki']="osłonka pochewki wewnętrznej";
$lang['warstwa_ziarnista']="warstwa nabłonkowa ziarnista (Huxleya)";
$lang['warstwa_jasna']="warstwa nabłonkowa jasna (Henlego)";
$lang['pochewka_zewnetrzna']="pochewka zewnętrzna";
$lang['brodawka_wlosa']="brodawka włosa";
$lang['cebulka_wlosa']="cebulka_wlosa";
$lang['Kod_decyduje']="Kod genetyczny decyduje nie tylko o naszym zdrowiu, ale i wyglądzie włosów. Cecha w postaci prostych lub kręconych włosów, powstaje ze względu na obecność genu nazwanego trichohyalinem. Jeśli jego równowaga aminokwasowa ulega zaburzeniu, włosy zaczynają się kręcić.";
$lang['Warto_podkreslic']="Warto przy tym podkreślić, że rodzaj włosów to cecha, która może zostać przekazana nie tylko w linii prostej. Oznacza to, że dziecko może mieć kręcone włosy nawet wtedy, gdy oboje rodziców mają włosy proste.";
$lang['jaki_jest_skręt']="Czy wiesz, jaki jest skręt Twojego włosa";
$lang['odpowiednia_pielegnacja']="Odpowiednia pielęgnacja włosów jest bardzo ważna, bo pozwala zadbać o ich kondycję i sprawić, że będą zawsze pięknie się prezentowały. Naocznie widzimy, jak proste lub poskręcane są nasze włosy, ale warto stopień skrętu sprawdzić dokładnie, a jest na to prosty sposób.";
$lang['Narysuj']="Narysuj kółko na kartce papieru o średnicy np. dna kubka,";
$lang['Wyrwij']="Wyrwij około 15 cm włos ze środka głowy,";
$lang['Zamocz']="Zamocz włos w wodzie i po wyjęciu odczekaj<br> aż całkowicie wyschnie";
$lang['Uloz']="Ułóż go na narysowanym obrysie kółka.";
$lang['mozesz_zaliczyc']="Twoje włosy możesz zaliczyć do prostych, jeśli w obrysie mieści się połowa włosa,";
$lang['obrys_kolka']="Jeśli w obrysie kółka zmieściło się przynajmniej 3/4 włosa – Twoje kosmki zalicza się do kręconych.";
$lang['Wyniki']="Wyniki:";
$lang['Powszechny_problem']="Powszechny problem wypadania włosów";
$lang['Opisane_uprzednio']="Opisane uprzednio fazy cyklu wzrostu włosa i ich ilość przypadająca na każdą z nich, są charakterystyczne dla osób zdrowych. U ludzi o zdrowych włosach około 85-90% jest w fazie anagenowej, a pozostałe 10-15% znajduje się w fazie telogenowej. Gdy dochodzi do zaburzeń w równowadze cyklu wzrostu włosów, wzmaga się wypadanie włosów i może dojść do stopniowego łysienia. Przyczyną takiego stanu mogą być:";
$lang['permanentny_stres']="permanentny stres,";
$lang['nieprawidlowa_dieta']="nieprawidłowa dieta,";
$lang['zaburzenia_hormonalne']="zaburzenia hormonalne,";
$lang['niewlasciwa_pielegnacja']="niewłaściwa pielęgnacja,";
$lang['genetyka']="genetyka.";
$lang['zaburzonym_cyklu']="Co dzieje się z włosami o zaburzonym cyklu wzrostu?";
$lang['proporcje_wlosow_anagenowych']="Proporcje włosów anagenowych i telogenowych ulegają zaburzeniu,";
$lang['wlosow_anagenowych']="Pojawia się coraz mniej włosów anagenowych, a systematycznie przybywa tych w fazie telogenu,";
$lang['Skraca_czas']="Skraca się czas trwania fazy anagenowej, co owocuje pojawianiem się włosów krótszych i cieńszych,";
$lang['zastepcze_wlosy']="Pojawiają się zastępcze włosy anagenowe, w skutek przedłużenia odstępu oddzielającego stratę a włos w fazie telogenowej";


//kontakt
$lang['Infolinia']="Infolinia:";
$lang['odwiedz_strone_producenta']="odwiedź stronę producenta";
$lang['czynnik_wzrostu']="FGF7, czynnik wzrostu fibroblastów 7, który promuje rozpowszechnianie macierzy keratynocytów na początku nowej fazy anagenowej.";

?>

