<?php
$subpage="marka";
$title="DuoLife Keratin Hair";
$background="inherit";
@include('inc/header.php');
?>

<body>
<div id="component">
    <?php @include('inc/top.php'); ?>
</div>
<div id="hair2">
    <div class="hair2-content">
        <div class="hair2-boxes">
            <div class="hair2-box-text">
                <h4><?php echo $lang['Dla_kogo_zostal'];?></h4>
                <div><?php echo $lang['Wypadanie_wlosów_to'];?>
                    <br><br>
                    <?php echo $lang['W_przypadku_procesu'];?><br><br>
                    <?php echo $lang['stworzylismy_po_to'];?><br><br>
                    <?php echo $lang['wplywaja_na_wzmocnienie'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hair3">
    <div class="hair3-line">
    </div>
    <div class="hair3-content">
        <div class="hair3-box">
            <div class="hair3-icon-box">
                <div>
                    <img  src="../img/hair3-img-ill.png" title="Dry hair" alt="Dry hair" />
                </div>
                <div class="hair3-box-text">
                    <?php echo $lang['Wlos_suchy'];?><br>&nbsp;
                </div>
            </div>
            <div class="hair3-icon-box">
                <div>
                    <img  src="../img/hair3-img-healthy.png" title="Normal hair" alt="Normal hair" />
                </div>
                <div class="hair3-box-text">
                    <?php echo $lang['Wlos_zdrowy'];?><br>&nbsp;
                </div>
            </div>
            <div class="hair3-icon-box">
                <div>
                    <img  src="../img/hair3-img-hair.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
                </div>
                <div class="hair3-box-text">
                    <b>DuoLife Keratin<br>Hair Complex</b>
                </div>
            </div>

        </div>
    </div>


    <?php @include('inc/footer.php'); ?>
</body>
</html>