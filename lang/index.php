<?php

$subpage="main";
$title="Keratin Hair-Complex - Strona Główna";
@include('inc/header.php');
?>

<body>
<div id="main">
    <?php @include('inc/top.php'); ?>
    <div class="main1-content">
        <div class="main1-img">
            <img  src="../img/main1-bottle.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
        </div>
        <div class="main1-text">
            <h3>
                DuoLife Keratin<br>
                Hair Complex
            </h3>
            <h4>
                Keratin Therapy. For your internal strength
            </h4>
            <div>
                <?php echo $lang['to_kontynuacja_produktow'];?>
            </div><br>
            <div class="main1-przypis">
                * <?php echo $lang['dotyczy_wybranych_składnikow'];?>
            </div>
        </div>

    </div>
    <div class="main2-content">
        <div class="main2-boxes">
            <div class="main2-box">
                <div class="main2-text">
                    <b><?php echo $lang['zdrowie_i_piekno'];?></b>
                </div>
                <div class="main2-icon-boxes">
                    <div class="main2-icon-box">
                        <div>
                            <img  src="../img/main2-34percent.png" title="34 percent" alt="34 percent" />
                        </div>
                        <div class="main2-box-text">
                            <div><?php echo $lang['Wypadanie_Twoich_wlosow'];?></div>
                        </div>
                    </div>
                    <div class="main2-icon-box">
                        <div>
                            <img  src="../img/main2-86percent.png" title="86 percent" alt="86 percent" />
                        </div>
                        <div class="main2-box-text">
                            <div><?php echo $lang['Znaczna_redukcja_wypadania'];?></div>
                        </div>
                    </div>
                    <div class="main2-icon-box">
                        <div>
                            <img  src="../img/main2-icon10.png" title="10x stronger and 2x more" alt="10x stronger and 2x more" />
                        </div>
                        <div class="main2-box-text">
                            <div><?php echo $lang['Twoje_wlosy_sa'];?></div>
                        </div>
                    </div>
                    <div class="main2-icon-box main2-icon4">
                        <div>
                            <img  src="../img/main2-iconXXL.png" title="XXL" alt="XXL" />
                        </div>
                        <div class="main2-box-text">
                            <div><?php echo $lang['Znaczna_poprawa_objetosci'];?></div>
                        </div>
                    </div>
                    <div class="main2-icon-box main2-icon5">
                        <div>
                            <img  src="../img/main2-icon3.png" width="192px" height="192px" title="3 months" alt="3 months" />
                        </div>
                        <div class="main2-box-text">
                            <div><?php echo $lang['Rezultaty_dzialania_widoczne'];?></div>
                        </div>
                    </div>
                </div>
                <div class="main2-przypis">
                    * <?php echo $lang['Badania_przeprowadzone_na'];?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main3-content">
    <div class="main3-boxes">
        <div class="main3-box">
            <div class="main3-text">
                <?php echo $lang['Poznaj_zalety_Keratin'];?>
            </div>
            <div class="main3-icon-boxes">
                <div class="main3-icon-box">
                    <div>
                        <img  src="../img/icon1-keratin.png" title="Liquid Keratin" alt="Liquid Keratin" />
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['zawiera_plynna_kreatynę'];?>
                    </div>
                </div>
                <div class="main3-icon-box">
                    <div>
                        <img  src="../img/icon2-keratin.png" title="Clinical Efficacy" alt="Clinical Efficacy" />
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['kliniczna_skutecznosc_icon'];?>
                    </div>
                </div>
                <div class="main3-icon-box">
                    <div>
                        <img  src="../img/icon3-keratin.png" title="International Patents" alt="International Patents" />
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['surowce_posiadaja_patenty'];?>
                    </div>
                </div>
                <div class="main3-icon-box">
                    <div>
                        <img  src="../img/icon4-keratin.png" title="Anti-Aging Complex" alt="iAnti-Aging Complex" />
                    </div>
                    <div class="main3-box-text">
                        Anti-Aging Complex
                    </div>
                </div>
                <div class="main3-icon-box main3-icon5">
                    <div>
                        <img  src="../img/icon5-keratin.png" title="100% natural" alt="100% natural" />
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['naturalny'];?>
                    </div>
                </div>
                <div class="main3-icon-box">
                    <div>
                        <img  src="../img/icon6-keratin.png" title="No preservatives" alt="No preservatives" />
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['nie_zawiera_substancji'];?>
                    </div>
                </div>
                <div class="main3-icon-box main3-icon7">
                    <div>
                        <img  src="../img/icon7-keratin.png" title="100% GMO Free" alt="100% GMO Free"/>
                    </div>
                    <div class="main3-box-text">
                        <?php echo $lang['nie_zawiera_produktow'];?>
                    </div>
                </div>
            </div>
            <div class="main3-przypis">
                * <?php echo $lang['dotyczy_wybranych_składnikow'];?>
            </div>
        </div>
    </div>
</div>
<div class="main4-content">
    <div class="main4-boxes">
        <div class="main4-box">
            <div class="main4-box-text">
                <h4>
                    <?php echo $lang['zawiera_naturalnych_skladnikow'];?>
                </h4>
                <p>
                    <?php echo $lang['owoce_gujawy'];?>
                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['owoce_amli'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['keratyna_solubilizowana'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['pestki_winogron'];?>
                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['czarny_ryz'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['groch'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['pokrzywa'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['skrzyp_polny'];?>

                </p>
                <div class="line-img"></div>
                <p>
                    <?php echo $lang['maliny'];?>

                </p>
                <div class="line-img"></div>
            </div>
            <div class="main4-img">
                <img  src="../img/main4-droplet.png" title="drop" alt="drop" />
            </div>
        </div>
    </div>
</div>
<div class="main5">
    <div class="main5-content">
        <div class="main5-boxes">
            <div class="main5-box">
                <div class="main5-img">
                    <img  src="../img/main5-bottle.png" title="DuoLife Keratin Hair Complex" alt="DuoLife Keratin Hair Complex" />
                </div>
                <div class="main5-box-text">
                    <h3>DuoLife Keratin<br>Hair Complex</h3>
                    <h4><?php echo $lang['plynna_keratyna'];?></h4>
                    <div class="main5-button-box">
                        <div class="main5-button-buy">
                            <a class="button-white" href="https://myduolife.com/shop/categories/1,1,produkty-duolife.html" target="_blank"><?php echo $lang['Kup_teraz'];?></a>
                        </div>
                    </div>
                    <div class="main5-button-box">
                        <div class="main5-button-leaflet">
                            <a class="button-dark" href="ulotka-duolife-keratin-hair-complex-i-en.pdf" target="_blank"><?php echo $lang['pobierz_ulotke'];?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php @include('inc/footer.php'); ?>
</body>
</html>