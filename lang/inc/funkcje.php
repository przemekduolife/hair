<?php
function clear($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function losowy_link($dlugosc) {

    $znaki= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    $losowy_ciag="";

    for ($i=0; $i < $dlugosc; $i++)
    {


        $losowy_ciag .= substr($znaki, rand(0, strlen($znaki)-1), 1);
    }


    return $losowy_ciag;
}