<div class="footer-container">
    <div class="footer-content">
        <div class="footer-box-logo">
            <div class="footer-logo">
                <img  src="../img/logo-keratin.svg" title="Logo Keratin Hair Complex" alt="Logo Keratin Hair Complex" />
            </div>
            <div class="footer-box-socialmedia">


               <a href="https://www.facebook.com/duolifepolska/" target = "_blanc"> <div class="footer-facebook"><i class="fa fa-facebook" style="font-size:24px;color:white"></i></div></a>
                <a href="https://www.instagram.com/life.duo/?hl=pl" target = "_blanc"><div class="footer-youtube"><i class="fa fa-instagram" style="font-size:24px;color:white"></i></div></a>
                <a href="https://www.youtube.com/channel/UCquhlH3_Oju24c8SSkJDymw" target = "_blanc"><div class="footer-youtube"><i class="fa fa-youtube-play" style="font-size:24px;color:white"></i></div></a>
                <a href="#header-smooth" ><div class="footer-top"><i class="fa fa-chevron-up" style="font-size:24px;color:white"></i></div></a>
            </div>

        </div>

        <div class="footer-box-producer">
            <div class="footer-product-for">
                <b><?php echo $lang['wyprodukowano'];?></b><br>
                Duolife S.A.<br>ul. TopolowA 22,<br>Więckowice k/Krakowa
            </div>

            <div class="footer-menu">
                <a href="index.php"><?php echo $lang['strona_glowna'];?></a>
                <a href="keratin-hair.php">Keratin Hair Complex</a>
                <a href="sklad.php"><?php echo $lang['sklad'];?></a>
                <a href="hair.php"><?php echo $lang['wlosy'];?></a>
                <a href="https://myduolife.com/shop/products/1/780,duolife-keratin-hair-complex.html" target="_blank"><?php echo $lang['kup_teraz'];?></a>
            </div>
        </div>
    </div>
    <div class = "stopka">
        <p>Copyright 2019. All rights reserved DuoLife.</p>
    </div>

    <div style="height: 60px;"></div>
</div>
<script src="../js/draggingSlider.js"></script>





